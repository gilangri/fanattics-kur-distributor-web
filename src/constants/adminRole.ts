export const role = {
  SUPER_ADMIN: "super",
  SALES_ADMIN: "sales",
  PRODUCT_ADMIN: "product",
};

export const adminRole = {
  ...role,
  navbarOptions: [role.SALES_ADMIN, role.PRODUCT_ADMIN],
};
