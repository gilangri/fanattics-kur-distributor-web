/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useEffect } from "react";
import NavBar from "./components/Navbar";
import { AppRoutes } from "./routes";
import { Switch, Route, useHistory, Redirect } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { SIDEBAR_SELECT } from "./redux/sidebar/types";
import { RootState } from "./redux/store";
import Login from "./pages/Login";
import { ReloginAction } from "./redux/auth/actions";

export default function App() {
  const dispatch = useDispatch();
  const history = useHistory();

  const isLogin = useSelector((state: RootState) => state.auth.user);
  const sidebarRole = useSelector((state: RootState) => state.auth.user?.role);

  useEffect(() => {
    if (!sidebarRole) {
      let title = "Login";
      let slug = "/login";
      dispatch({ type: SIDEBAR_SELECT, payload: { title, slug } });
      history.push(slug);
    } else {
      let title = "Transaksi";
      let slug = "/transaction/on-process";

      // title = "Katalog Produk";
      // slug = "/product/catalog";
      dispatch({ type: SIDEBAR_SELECT, payload: { title, slug } });
      history.push(slug);
    }
  }, [dispatch, history, sidebarRole]);

  useEffect(() => {
    let token = localStorage.getItem("token");
    if (token) dispatch(ReloginAction(token));
  }, [dispatch]);

  if (!isLogin)
    return (
      <div className="App-wrapper">
        <Route path="/" exact component={Login} />
        <Route path="/login" component={Login} />
        <Redirect from="*" to="/" />s
      </div>
    );

  return (
    <React.Fragment>
      <NavBar />

      <div className="App-wrapper">
        {AppRoutes.map((route) => (
          <Route
            key={route.path}
            path={route.path}
            exact={route.exact}
            component={route.sidebar}
          />
        ))}

        <Switch>
          {AppRoutes.map((route) => (
            <Route
              key={route.path}
              path={route.path}
              exact={route.exact}
              component={route.main}
            />
          ))}
        </Switch>
      </div>
    </React.Fragment>
  );
}
