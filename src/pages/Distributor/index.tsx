/* eslint-disable @typescript-eslint/no-unused-vars */
import "./distributor.scss";

import { UilUserPlus } from "@iconscout/react-unicons";
import {
  Button,
  ButtonIcon,
  ContainerBody,
  ContainerBodyContent,
  ContainerBodyHead,
  ContainerHead,
  Pagination,
  Panel,
  ParentContainer,
  Searchbar,
  Table,
  TableBody,
  TableCell,
  TableFooter,
  TableHead,
  TableHeadCell,
  TableRow,
  usePagination,
} from "../../components";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../redux/store";
import { DISTRIBUTOR } from "./state/interfaces/distributor";
import { useEffect } from "react";
import { filterDistributor, getDistributor } from "./state/actions/distributor";
import { IDistributor } from "../../redux/_interface/distributor";
import { useHistory } from "react-router-dom";

export default function DistributorPage() {
  const dispatch = useDispatch();
  const history = useHistory();

  const { data, filtered, searchQuery, lastUpdate, loading } = useSelector(
    (state: RootState) => state.distributor
  );

  const { next, prev, jump, currentData, pages, currentPage, maxPage } =
    usePagination({
      data: filtered,
      itemsPerPage: 10,
    });

  useEffect(() => {
    dispatch({ type: DISTRIBUTOR.SET_VALUE, name: "data", payload: [] });
    dispatch(getDistributor());
  }, [dispatch, lastUpdate]);

  useEffect(() => {
    dispatch(filterDistributor(data, searchQuery));
  }, [data, dispatch, searchQuery]);

  return (
    <ParentContainer title="Daftar Distributor">
      <ContainerHead
        title="Daftar Distributor"
        onRefresh={() =>
          dispatch({
            type: DISTRIBUTOR.SET_VALUE,
            name: "lastUpdate",
            payload: new Date(),
          })
        }
        refreshing={loading}
      >
        <Button theme="primary" onClick={() => history.push("/distributor/form")}>
          <ButtonIcon>
            <UilUserPlus />
          </ButtonIcon>
          &nbsp; Tambah Distributor
        </Button>
      </ContainerHead>

      <ContainerBody>
        <Panel>
          <ContainerBodyHead className="row mb-4">
            <div className="col-3">
              <Searchbar
                autoFocus
                placeholder="Cari Admin"
                value={searchQuery}
                onChange={(payload) =>
                  dispatch({
                    type: DISTRIBUTOR.SET_VALUE,
                    name: "searchQuery",
                    payload,
                  })
                }
              />
            </div>
          </ContainerBodyHead>

          <ContainerBodyContent>
            <Table>
              <TableHead>
                <TableRow>
                  <TableHeadCell style={{ width: "50%" }} name="name">
                    Nama Distributor
                  </TableHeadCell>
                  <TableHeadCell style={{ width: "30%" }} name="pinterId">
                    User ID
                  </TableHeadCell>
                  <TableHeadCell style={{ width: "20%" }} name="phoneNumber">
                    Nomor Telepon
                  </TableHeadCell>
                </TableRow>
              </TableHead>

              <RenderBody data={currentData()} />

              <TableFooter>
                <TableRow>
                  <TableCell colSpan={10} align="center">
                    <Pagination
                      next={next}
                      prev={prev}
                      jump={jump}
                      pages={pages}
                      currentPage={currentPage}
                      maxPage={maxPage}
                    />
                  </TableCell>
                </TableRow>
              </TableFooter>
            </Table>
          </ContainerBodyContent>
        </Panel>
      </ContainerBody>
    </ParentContainer>
  );
}

const RenderBody = ({
  data,
  onClick,
}: {
  data: IDistributor[];
  onClick?: (item: IDistributor) => void;
}) => {
  return (
    <TableBody>
      {!data.length
        ? null
        : data.map((item, i) => {
            return (
              <TableRow key={i} onClick={() => onClick && onClick(item)}>
                <TableCell style={{ width: "50%" }}>{item.name}</TableCell>
                <TableCell style={{ width: "30%" }}>{item._id}</TableCell>
                <TableCell style={{ width: "20%" }}>{item.phoneNumber}</TableCell>
              </TableRow>
            );
          })}
    </TableBody>
  );
};
