import { AnyAction } from "redux";
import { IBank, ISharingPartner } from "../../../../redux/_interface";

export interface IDistributorFormState {
  banks: IBank[];
  sharingPartners: ISharingPartner[];

  name: string;
  email: string;
  phoneNumber: string;
  profilePicture?: File;
  bank: string;
  bankAccountNumber: string;
  partners: IPartnerForm[];
  representatives: IRepresentativeForm[];
}

export enum DISTRIBUTOR_FORM {
  RESET = "DISTRIBUTOR_FORM_RESET",
  SET = "DISTRIBUTOR_FORM_SET",
  SET_VALUE = "DISTRIBUTOR_FORM_SET_VALUE",

  CHANGE_PARTNER = "DISTRIBUTOR_FORM_CHANGE_PARTNER",
  CHANGE_REVENUE_SHARE_VALUE = "DISTRIBUTOR_FORM_CHANGE_REVENUE_SHARE_VALUE",
  ADD_PARTNER = "DISTRIBUTOR_FORM_ADD_PARTNER",
  REMOVE_PARTNER = "DISTRIBUTOR_FORM_REMOVE_PARTNER",

  CHANGE_REPRESENTATIVE_VALUE = "DISTRIBUTOR_FORM_CHANGE_REPRESENTATIVE_VALUE",
  ADD_REPRESENTATIVE = "DISTRIBUTOR_FORM_ADD_REPRESENTATIVE",
  REMOVE_REPRESENTATIVE = "DISTRIBUTOR_FORM_REMOVE_REPRESENTATIVE",
}

export interface IDistributorFormAction extends AnyAction {
  payload: any;
  name: string;
  id: string;
  type: DISTRIBUTOR_FORM;
}

interface IPartnerForm {
  id: string;
  sharingPartner: ISharingPartner | string;
  name: string;
  initialCode: string;
  revenueSharePercentage: number;
  active: boolean;
}

interface IRepresentativeForm {
  id: string;
  distributor: string;
  firstName: string;
  lastName: string;
  phoneNumber: string;
  email: string;
  jobPosition: string;
}
