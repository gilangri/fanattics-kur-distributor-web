import { AnyAction } from "redux";
import { IDistributor } from "../../../../redux/_interface/distributor";

export interface IDistributorState {
  data: IDistributor[];
  filtered: IDistributor[];

  item?: IDistributor;

  searchQuery: string;
  lastUpdate: Date;

  loading: boolean;
  error: string;
}

export enum DISTRIBUTOR {
  REQUEST = "DISTRIBUTOR_REQUEST",
  FAILED = "DISTRIBUTOR_FAILED",
  RESET = "DISTRIBUTOR_RESET",
  SET_DISTRIBUTORS = "DISTRIBUTOR_SET_DISTRIBUTORS",
  SET_DISTRIBUTOR = "DISTRIBUTOR_SET_DISTRIBUTOR",
  SET = "DISTRIBUTOR_SET",
  SET_VALUE = "DISTRIBUTOR_SET_VALUE",
}

export interface IDistributorAction extends AnyAction {
  payload: any;
  name: string;
  type: DISTRIBUTOR;
}
