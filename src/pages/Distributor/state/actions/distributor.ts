import { IDistributor } from "../../../../redux/_interface/distributor";
import { sortArrByKey } from "../../../../utils";
import { DISTRIBUTOR } from "../interfaces/distributor";
import { dummy } from "./dummy";

export const getDistributor =
  (id: string = "") =>
  async (dispatch: any) => {
    try {
      dispatch({ type: DISTRIBUTOR.REQUEST });

      if (id) {
        const { data } = dummy;
        const selected = data.result.filter((item) => item._id === id);
        const payload = data.result.length ? selected[0] : undefined;

        setTimeout(() => {
          dispatch({ type: DISTRIBUTOR.SET_DISTRIBUTOR, payload });
        }, 1000);
      } else {
        const { data } = dummy;
        const payload = sortArrByKey(data.result, "name");

        setTimeout(() => {
          dispatch({ type: DISTRIBUTOR.SET_DISTRIBUTORS, payload });
        }, 1000);
      }
    } catch (err) {
      console.error(err);
      dispatch({ type: DISTRIBUTOR.FAILED, payload: err });
    }
  };

/**
 * Filter data by search query based on it's name
 * @param data fetched data
 * @param searchQuery inputted search query string
 * @returns filtered data based on searchQuery
 */
export const filterDistributor =
  (data: IDistributor[] = [], searchQuery: string = "") =>
  async (dispatch: any) => {
    const payload = !data.length
      ? []
      : data && !searchQuery
      ? data
      : data.filter((item) => {
          const query = searchQuery.toLowerCase();
          const name = item.name.toLowerCase();
          return name.includes(query);
        });

    dispatch({ type: DISTRIBUTOR.SET_VALUE, name: "filtered", payload });
  };
