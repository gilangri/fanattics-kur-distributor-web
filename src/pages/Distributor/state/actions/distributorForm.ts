import axios from "axios";
import { API_URL_PINTER } from "../../../../constants";
import { DISTRIBUTOR_FORM } from "../interfaces/distributorForm";

export const getDefaultData = () => async (dispatch: any) => {
  try {
    const resBanks = await axios.get(`${API_URL_PINTER}/banks`);
    const resSharingPartners = await axios.get(`${API_URL_PINTER}/partners`);

    dispatch({
      type: DISTRIBUTOR_FORM.SET,
      payload: {
        banks: resBanks.data.result,
        sharingPartners: resSharingPartners.data.result,
      },
    });
  } catch (err) {
    console.error(err);
  }
};
