import {
  DISTRIBUTOR,
  IDistributorAction,
  IDistributorState,
} from "../interfaces/distributor";

const init_state: IDistributorState = {
  data: [],
  filtered: [],

  item: undefined,

  searchQuery: "",
  lastUpdate: new Date(),

  loading: false,
  error: "",
};

export function distributorReducer(
  state = init_state,
  { payload, name, type }: IDistributorAction
): IDistributorState {
  switch (type) {
    case DISTRIBUTOR.REQUEST:
      return { ...state, loading: true, error: "" };
    case DISTRIBUTOR.FAILED:
      return { ...state, loading: false, error: payload };

    case DISTRIBUTOR.SET_DISTRIBUTORS:
      return { ...state, loading: false, data: payload };
    case DISTRIBUTOR.SET_DISTRIBUTOR:
      return { ...state, loading: false, item: payload };

    case DISTRIBUTOR.SET:
      return { ...state, ...payload };
    case DISTRIBUTOR.SET_VALUE:
      return { ...state, [name]: payload };

    case DISTRIBUTOR.RESET:
      return init_state;
    default:
      return state;
  }
}
