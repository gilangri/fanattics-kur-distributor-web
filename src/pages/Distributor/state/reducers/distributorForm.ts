import {
  DISTRIBUTOR_FORM,
  IDistributorFormAction,
  IDistributorFormState,
} from "../interfaces/distributorForm";

const init_state: IDistributorFormState = {
  banks: [],
  sharingPartners: [],

  name: "",
  email: "",
  phoneNumber: "",
  profilePicture: undefined,
  bank: "",
  bankAccountNumber: "",
  partners: [
    {
      id: new Date().getMilliseconds().toString(),
      active: false,
      initialCode: "",
      name: "",
      revenueSharePercentage: 0,
      sharingPartner: "",
    },
  ],
  representatives: [
    {
      id: new Date().getMilliseconds().toString(),
      distributor: "",
      email: "",
      firstName: "",
      jobPosition: "",
      lastName: "",
      phoneNumber: "",
    },
  ],
};

export function distributorFormReducer(
  state = init_state,
  { payload, id, name, type }: IDistributorFormAction
): IDistributorFormState {
  switch (type) {
    case DISTRIBUTOR_FORM.SET:
      return { ...state, ...payload };
    case DISTRIBUTOR_FORM.SET_VALUE:
      return { ...state, [name]: payload };

    case DISTRIBUTOR_FORM.ADD_PARTNER:
      return {
        ...state,
        partners: [
          ...state.partners,

          // add new partner with all empty value
          {
            id: new Date().getMilliseconds().toString(),
            sharingPartner: "",
            name: "",
            initialCode: "",
            revenueSharePercentage: 0.0,
            active: false,
          },
        ],
      };
    case DISTRIBUTOR_FORM.REMOVE_PARTNER:
      return {
        ...state,

        // omit partner data with the same id with payload sent
        partners: state.partners.filter((val) => val.id !== id),
      };
    case DISTRIBUTOR_FORM.CHANGE_PARTNER:
      return {
        ...state,
        partners: state.partners.map((item) => {
          // if item's id different with id sent
          // then return as it is (skip the changes)
          if (item.id !== id) return item;

          // if item's id similar with id sent, then
          // find data from array sharingPartners with the same value
          // with value sent within payload
          const partner = state.sharingPartners.find(
            (e) => e._id === payload || undefined
          );

          // change partner info with new selected sharing partner
          return {
            ...item,
            sharingPartner: partner?._id || "",
            name: partner?.name || "",
            initialCode: partner?.initialCode || "",
          };
        }),
      };
    case DISTRIBUTOR_FORM.CHANGE_REVENUE_SHARE_VALUE:
      return {
        ...state,
        partners: state.partners.map((item) => {
          // if item's id different with id sent
          // then return as it is (skip the changes)
          if (item.id !== id) return item;

          // if item's id similar with id sent, then
          // change revenue share percentage with new value
          // default value always 0
          return {
            ...item,
            revenueSharePercentage: !payload ? 0 : parseInt(payload),
          };
        }),
      };

    case DISTRIBUTOR_FORM.ADD_REPRESENTATIVE:
      return {
        ...state,
        representatives: [
          ...state.representatives,

          // add new representative with all empty value
          {
            id: new Date().getMilliseconds().toString(),
            distributor: "",
            email: "",
            firstName: "",
            jobPosition: "",
            lastName: "",
            phoneNumber: "",
          },
        ],
      };
    case DISTRIBUTOR_FORM.REMOVE_REPRESENTATIVE:
      return {
        ...state,

        // omit representative with the same id with payload sent
        representatives: state.representatives.filter((val) => val.id !== id),
      };
    case DISTRIBUTOR_FORM.CHANGE_REPRESENTATIVE_VALUE:
      return {
        ...state,
        representatives: state.representatives.map((item) => {
          // if item's id different with id sent
          // then return as it is (skip the changes)
          if (item.id !== id) return item;

          // if item's id similar with id sent, then
          // change the value of name sent
          return { ...item, [name]: payload };
        }),
      };

    case DISTRIBUTOR_FORM.RESET:
      return init_state;
    default:
      return state;
  }
}
