/* eslint-disable @typescript-eslint/no-unused-vars */
import "./form.scss";

import { useDispatch, useSelector } from "react-redux";
import {
  ContainerBody,
  ContainerBodyContent,
  ContainerBodyHead,
  ContainerHead,
  Panel,
  ParentContainer,
} from "../../../components";
import { RootState } from "../../../redux/store";
import { useGenerateAddresses } from "../../../utils/hooks";
import PanelDetailDistributor from "./PanelDetail";
import PanelAddressDistributor from "./PanelAddress";
import { useEffect } from "react";
import { getDefaultData } from "../state/actions/distributorForm";
import PanelBankAccountDistributor from "./PanelBankAccount";
import PanelRepresentativeDistributor from "./PanelRepresentatives";
import PanelPartnersDistributor from "./PanelPartners";

export default function DistributorForm() {
  const dispatch = useDispatch();

  const addressGenerator = useGenerateAddresses();
  // const {
  //   address,
  //   changeAddress,
  //   provinces,
  //   provinceId,
  //   province,
  //   changeProvince,
  //   cities,
  //   cityId,
  //   city,
  //   changeCity,
  //   districts,
  //   districtId,
  //   district,
  //   changeDistrict,
  //   subdistricts,
  //   subdistrictId,
  //   subdistrict,
  //   changeSubdistrict,
  //   postalCode,
  //   changePostalCode,
  // } = addressGenerator;

  // const {
  //   bank,
  //   bankAccountNumber,
  //   banks,
  //   email,
  //   name,
  //   partners,
  //   phoneNumber,
  //   representatives,
  //   sharingPartners,
  //   profilePicture,
  // } = useSelector((state: RootState) => state.distributorForm);

  useEffect(() => {
    dispatch(getDefaultData());
  }, [dispatch]);

  return (
    <ParentContainer title="Tambah Distributor">
      <ContainerHead backButton />

      <ContainerBody>
        <Panel>
          <ContainerBodyHead>
            <h3>Tambah Distributor Baru</h3>
          </ContainerBodyHead>

          <ContainerBodyContent className="distributor-form">
            <PanelDetailDistributor />

            <PanelAddressDistributor hooks={addressGenerator} />

            <PanelBankAccountDistributor />

            <PanelRepresentativeDistributor />

            <PanelPartnersDistributor />
          </ContainerBodyContent>
        </Panel>
      </ContainerBody>
    </ParentContainer>
  );
}
