import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import {
  Button,
  Form,
  Input,
  Modal,
  ModalBody,
  ModalHeader,
} from "../../components";
import { RootState } from "../../redux/store";

interface Props {
  isOpen?: boolean;
  toggle?: () => void;
  closeButton?: boolean;
  handleSimpan?: (email: string) => void;
  defaultValue: string;
}

export const ModalEditEmail = ({
  isOpen,
  toggle,
  closeButton,
  handleSimpan,
  defaultValue,
}: Props) => {
  //* GLOBAL STATE
  const isLoading = useSelector((state: RootState) => state.distributor.loading);

  //* LOCAL STATE
  const [email, setEmail] = useState("");

  //* FUNCTION
  const onSimpan = async () => {
    if (!handleSimpan || isLoading || !defaultValue) return;
    try {
      await Promise.all([handleSimpan(email)]);
      setEmail("");
    } catch (err) {
      console.error(err);
    }
  };

  const onCancel = () => {
    if (!toggle) return;
    setEmail("");
    toggle();
  };

  useEffect(() => {
    setEmail(defaultValue);
  }, [defaultValue]);

  return (
    <Modal isOpen={isOpen} toggle={!closeButton ? toggle : undefined}>
      <ModalHeader title="Ganti Email" toggle={closeButton ? toggle : undefined} />
      <ModalBody className="popup-category">
        <Form className="input-category">
          <Input
            value={email}
            onChange={({ target }) => setEmail(target.value)}
            onKeyPress={(e) => e.key === "Enter" && e.preventDefault()}
          />
        </Form>

        <div className="button-action">
          <Button
            disabled={isLoading}
            onClick={onCancel}
            minWidth={150}
            className="mt-3"
            theme="secondary"
          >
            Batal
          </Button>
          <Button
            isLoading={isLoading}
            disabled={isLoading}
            onClick={onSimpan}
            minWidth={150}
            className="mt-3"
            theme="primary"
          >
            Simpan
          </Button>
        </div>
      </ModalBody>
    </Modal>
  );
};
