import { useEffect, useState } from "react";
import {
  Button,
  ContainerBody,
  ContainerHead,
  ParentContainer,
} from "../../components";
import { FiPhoneCall, FiMail } from "react-icons/fi";
import { HiPencil, HiOutlineLocationMarker } from "react-icons/hi";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../redux/store";
import { editDistributor, getDistributor } from "../../redux/profileSetting/actions";
import { ModalEditEmail } from "./_modalEditEmail";
import { ModalEditPhoneNumber } from "./_modalEditPhone";
import { ModalEditAddress } from "./_modalEditAdress";

export default function SettingProfile() {
  const dispatch = useDispatch();
  const users = useSelector((state: RootState) => state.distributors.user);
  const [editEmail, setEditEmail] = useState("");
  const [editPhoneNumber, setEditPhoneNumber] = useState("");
  const [editAddress, setEditAddress] = useState("");

  //* FETCH DATA
  useEffect(() => {
    dispatch(getDistributor());
  }, [dispatch]);

  const handleEditEmail = async (email: string) => {
    try {
      await Promise.all([dispatch(editDistributor({ email }))]);
      setEditEmail("");
    } catch (err) {
      console.error(err);
    }
  };

  const handleEditPhoneNumber = async (phoneNumber: string) => {
    try {
      await Promise.all([dispatch(editDistributor({ phoneNumber }))]);
      setEditPhoneNumber("");
    } catch (err) {
      console.error(err);
    }
  };

  const handleEditAddress = async (val: string) => {
    try {
      const formdata = new URLSearchParams();
      formdata.append("address", val);
      await Promise.all([dispatch(editDistributor(formdata))]);
      setEditAddress("");
    } catch (err) {
      console.error(err);
    }
  };

  return (
    <ParentContainer title="Profile Setting" className="profile-setting">
      <ModalEditEmail
        defaultValue={editEmail}
        isOpen={editEmail !== ""}
        toggle={() => setEditEmail("")}
        handleSimpan={(e) => handleEditEmail(e)}
      />

      <ModalEditPhoneNumber
        defaultValue={editPhoneNumber}
        isOpen={editPhoneNumber !== ""}
        toggle={() => setEditPhoneNumber("")}
        handleSimpan={(e) => handleEditPhoneNumber(e)}
      />

      <ModalEditAddress
        defaultValue={editAddress}
        isOpen={editAddress !== ""}
        toggle={() => setEditAddress("")}
        handleSimpan={handleEditAddress}
      />

      <ContainerHead title="App Setting & Profile" titleStyle={{ bold: true }} />
      <ContainerBody className="d-flex justify-content-between">
        <div className="panel-container">
          <div className="edit-container">
            <FiPhoneCall size={40} color="#3AA0F3" />

            <Button
              className="button-edit"
              onClick={() => setEditPhoneNumber(users?.phoneNumber || "")}
            >
              <HiPencil />
            </Button>
          </div>
          <div className="information-account">
            <span className="text-head">Phone Number</span>
            <span className="text-child">{users?.phoneNumber}</span>
          </div>
        </div>
        <div className="panel-container">
          <div className="edit-container">
            <FiMail size={40} color="#F38D3A" />

            <Button
              className="button-edit"
              onClick={() => setEditEmail(users?.email || "")}
            >
              <HiPencil />
            </Button>
          </div>
          <div className="information-account">
            <span className="text-head">Email Address</span>
            <span className="text-child">{users?.email}</span>
          </div>
        </div>
        <div className="panel-container">
          <div className="edit-container">
            <HiOutlineLocationMarker size={40} color="#009F98" />

            {/* <Button
              className="button-edit"
              onClick={() =>
                setEditAddress(users?.address || "")
              }
            >
              <HiPencil />
            </Button> */}
          </div>
          <div className="information-account">
            <span className="text-head">Address</span>
            <span className="text-child">{users?.address}</span>
          </div>
        </div>
      </ContainerBody>
    </ParentContainer>
  );
}

// const RenderBody = ({ data }: { data?: IDistributor }) => {
//   return (
//     <ContainerBody className="d-flex justify-content-between">
//       <div className="panel-container">
//         <div className="edit-container">
//           <FiPhoneCall size={40} color="#3AA0F3" />

//           <button className="button-edit">
//             <HiPencil />
//           </button>
//         </div>
//         <div className="information-account">
//           <span className="text-head">Phone Number</span>
//           <span className="text-child">{data?.phoneNumber}</span>
//         </div>
//       </div>
//       <div className="panel-container">
//         <div className="edit-container">
//           <FiMail size={40} color="#F38D3A" />

//           <button className="button-edit">
//             <HiPencil />
//           </button>
//         </div>
//         <div className="information-account">
//           <span className="text-head">Email Address</span>
//           <span className="text-child">{data?.email}</span>
//         </div>
//       </div>
//       <div className="panel-container">
//         <div className="edit-container">
//           <HiOutlineLocationMarker size={40} color="#009F98" />

//           <button className="button-edit">
//             <HiPencil />
//           </button>
//         </div>
//         <div className="information-account">
//           <span className="text-head">Address</span>
//           <span className="text-child">{data?.address}</span>
//         </div>
//       </div>
//     </ContainerBody>
//   );
// };
