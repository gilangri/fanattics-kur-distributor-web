import { IAdmin } from "../../../redux/_interface/admin";

export interface IAdminState {
  isOpen: boolean;
  isDeleting: boolean;
  isEditing?: IAdmin;

  admins: IAdmin[];
  admin?: IAdmin;

  filtered: IAdmin[];

  searchQuery: string;
  searched: IAdmin[];
  isSearching: boolean;

  loading: boolean;
  error: string;
}

export enum ADMIN {
  REQUEST = "ADMIN_REQUEST",
  FAILED = "ADMIN_FAILED",
  RESET = "ADMIN_RESET",
  SEARCH = "ADMIN_SEARCH",
  SEARCH_DONE = "ADMIN_SEARCH_DONE",
  SEARCH_RESET = "ADMIN_SEARCH_RESET",
  SET_ADMINS = "ADMIN_SET_ADMINS",
  SET_ADMIN = "ADMIN_SET_ADMIN",
  SET = "ADMIN_SET",
  SET_VALUE = "ADMIN_SET_VALUE",
  TOGGLE_IS_OPEN = "ADMIN_TOGGLE_IS_OPEN",
  HANDLE_EDIT = "ADMIN_HANDLE_EDIT",
  CANCEL_EDIT = "ADMIN_CANCEL_EDIT",
  HANDLE_DELETE = "ADMIN_HANDLE_DELETE",
  CANCEL_DELETE = "ADMIN_CANCEL_DELETE",
}

export interface IAdminAction {
  payload: any;
  name: string;
  type: ADMIN;
}
