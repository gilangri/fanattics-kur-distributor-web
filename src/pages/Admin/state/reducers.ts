import { IAdminAction, IAdminState, ADMIN } from "./interfaces";

const init_state: IAdminState = {
  isOpen: false,
  isDeleting: false,
  isEditing: undefined,

  admins: [],
  admin: undefined,

  filtered: [],

  searchQuery: "",
  searched: [],
  isSearching: false,

  loading: false,
  error: "",
};

export function adminStateReducer(
  state = init_state,
  { payload, name, type }: IAdminAction
): IAdminState {
  switch (type) {
    case ADMIN.REQUEST:
      return { ...state, loading: true, error: "" };
    case ADMIN.FAILED:
      return { ...state, loading: false, error: payload };

    case ADMIN.SEARCH:
      return { ...state, isSearching: true };
    case ADMIN.SEARCH_DONE:
      return { ...state, searched: payload };
    case ADMIN.SEARCH_RESET:
      return { ...state, isSearching: false, searched: [] };

    case ADMIN.SET_ADMINS:
      return { ...state, loading: false, admins: payload };
    case ADMIN.SET_ADMIN:
      return { ...state, loading: false, admin: payload };

    case ADMIN.SET:
      return { ...state, ...payload };
    case ADMIN.SET_VALUE:
      return { ...state, [name]: payload };

    case ADMIN.TOGGLE_IS_OPEN:
      return { ...state, isOpen: !state.isOpen, isEditing: undefined };
    case ADMIN.HANDLE_EDIT:
      return { ...state, isOpen: true, isEditing: payload };
    case ADMIN.CANCEL_EDIT:
      return { ...state, isOpen: false, isEditing: undefined };

    case ADMIN.HANDLE_DELETE:
      return { ...state, isDeleting: true };
    case ADMIN.CANCEL_DELETE:
      return { ...state, isDeleting: false };

    case ADMIN.RESET:
      return init_state;
    default:
      return state;
  }
}
