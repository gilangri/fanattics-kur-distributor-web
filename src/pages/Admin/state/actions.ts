import axios from "axios";
import { API_URL } from "../../../constants";
import { IAdmin } from "../../../redux/_interface";
import { sortArrByKey } from "../../../utils";
import { ADMIN } from "./interfaces";

/**
 * fetching admin(s) data
 * @param id (optional) - provide id to get singular admin
 */
export const getAdmin =
  (id: string = "") =>
  async (dispatch: any) => {
    try {
      dispatch({ type: ADMIN.REQUEST });
      if (id) {
        const { data } = await axios.get(`${API_URL}/admins/${id}`);
        const selected = data.result.filter((val: IAdmin) => val._id === id);
        const payload = selected.length ? selected[0] : undefined;
        dispatch({ type: ADMIN.SET_ADMIN, payload });
      } else {
        const { data } = await axios.get(`${API_URL}/admins/${id}`);
        const payload = sortArrByKey(data.result, "name");
        dispatch({ type: ADMIN.SET_ADMINS, payload });
      }
    } catch (err) {
      console.error(err);
      dispatch({ type: ADMIN.FAILED, payload: err });
    }
  };

/**
 * create new admin
 * @param body body of new admin
 */
export const createAdmin = (body: Object) => async (dispatch: any) => {
  try {
    dispatch({ type: ADMIN.REQUEST });
    const { data } = await axios.post(`${API_URL}/admins`, body);
    console.log(data);
    if (data.result.hasOwnProperty("errors")) throw new Error(data.result.message);
    await Promise.all([dispatch(getAdmin())]);
  } catch (err) {
    console.log("error create");
    console.error(err);
    dispatch({ type: ADMIN.FAILED, payload: err });

    throw new Error(err);
  }
};

/**
 * Edit admin information
 * @param id selected admin's id
 * @param body body of edited admin
 */
export const editAdmin = (id: string, body: Object) => async (dispatch: any) => {
  try {
    dispatch({ type: ADMIN.REQUEST });
    await axios.put(`${API_URL}/admins/${id}`, body);
    await Promise.all([dispatch(getAdmin())]);
  } catch (err) {
    console.log("error edit");
    console.error(err);
    dispatch({ type: ADMIN.FAILED, payload: err });
  }
};

/**
 * Delete admin by its id
 * @param id deleting admin id
 */
export const deleteAdmin = (id: string) => async (dispatch: any) => {
  try {
    dispatch({ type: ADMIN.REQUEST });
    await axios.delete(`${API_URL}/admins/${id}`);
    await Promise.all([dispatch(getAdmin())]);
    dispatch({ type: ADMIN.CANCEL_DELETE });
    dispatch({ type: ADMIN.CANCEL_EDIT });
  } catch (err) {
    console.error(err);
    dispatch({ type: ADMIN.FAILED, payload: err });
  }
};

/**
 * Handler for create new admin or save edited admin if second parameter is provided
 * @param body object of new created admin
 * @param isEditing (optional) - currently edit admin data
 */
export const handleSaveAdmin =
  (body: Object, isEditing: IAdmin | undefined = undefined) =>
  async (dispatch: any) => {
    try {
      await Promise.all(
        isEditing
          ? [dispatch(editAdmin(isEditing._id, body))]
          : [dispatch(createAdmin(body))]
      );
      console.log("saved");

      dispatch({ type: ADMIN.CANCEL_EDIT });
    } catch (err) {
      console.log("error save");
      console.error(err);
      throw new Error(err);
    }
  };

/**
 * Filter data by search query based on it's name
 * @param data fetched data
 * @param searchQuery inputted search query string
 * @param sortedBy parameter for sorting data
 * @returns filtered data based on searchQuery
 */
export const filterAdmin =
  (data: IAdmin[] = [], searchQuery: string = "", sortedBy: string) =>
  async (dispatch: any) => {
    const payload = !data.length
      ? []
      : data && !searchQuery
      ? data
      : data.filter((item) => {
          const query = searchQuery.toLowerCase();
          const name = item.name.toLowerCase();
          return name.includes(query);
        });

    const objectKey = sortedBy.split("-")[0];
    const inverse = sortedBy.split("-")[1] === "asc";

    const sorted = !payload.length ? [] : sortArrByKey(payload, objectKey, inverse);

    dispatch({ type: ADMIN.SET_VALUE, name: "filtered", payload: sorted });
  };
