/* eslint-disable @typescript-eslint/no-unused-vars */
import "./admin.scss";

import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  Table,
  Panel,
  TableBody,
  TableCell,
  TableHead,
  TableHeadCell,
  TableRow,
  Searchbar,
  ParentContainer,
  ContainerHead,
  ContainerBody,
  TableFooter,
  usePagination,
  Pagination,
  Button,
  ContainerBodyContent,
  ContainerBodyHead,
} from "../../components";
import { RootState, IAdmin } from "../../redux/store";
import {
  deleteAdmin,
  filterAdmin,
  getAdmin,
  handleSaveAdmin,
} from "./state/actions";
import { ADMIN } from "./state/interfaces";
import { PopupManageAdmin } from "./_popupManageAdmin";
import Confirmation from "../../components/Popup/Confirmation";

export default function AdminPage() {
  const dispatch = useDispatch();

  //* GLOBAL STATE
  const { admins, filtered, isOpen, isDeleting, isEditing, searchQuery, loading } =
    useSelector((state: RootState) => state.admin);

  const [sortedBy, setSortedBy] = useState("name-desc");

  const { next, prev, jump, currentData, pages, currentPage, maxPage } =
    usePagination({
      data: filtered,
      itemsPerPage: 10,
    });

  //* FETCH DATA
  useEffect(() => {
    dispatch(filterAdmin(admins, searchQuery, sortedBy));
  }, [admins, dispatch, searchQuery, sortedBy]);

  useEffect(() => {
    dispatch(getAdmin());
  }, [dispatch]);

  return (
    <ParentContainer title="Daftar Admin">
      <PopupManageAdmin
        editingData={isEditing}
        isOpen={isOpen}
        toggle={() => dispatch({ type: ADMIN.CANCEL_EDIT })}
        isLoading={!isDeleting && isEditing && loading}
        handleSimpan={(item) => dispatch(handleSaveAdmin(item, isEditing))}
        handleDelete={
          !isEditing ? undefined : () => dispatch({ type: ADMIN.HANDLE_DELETE })
        }
      />
      <Confirmation
        isOpen={isDeleting}
        title={`Hapus ${isEditing?.name}?`}
        onCancel={() => dispatch({ type: ADMIN.CANCEL_DELETE })}
        isLoading={isDeleting && isEditing && loading}
        onConfirm={() => isEditing && dispatch(deleteAdmin(isEditing._id))}
        confirmButtonTheme="danger"
      />

      <ContainerHead title="Daftar Admin" />

      <ContainerBody>
        <Panel>
          <ContainerBodyHead className="admin-catalog row mb-3">
            <div className="col-9">
              <div className="body-title">Total User: {admins.length}</div>
            </div>

            <div className="col-3">
              <Searchbar
                autoFocus
                placeholder="Cari Admin"
                value={searchQuery}
                onChange={(payload) =>
                  dispatch({ type: ADMIN.SET_VALUE, name: "searchQuery", payload })
                }
              />
            </div>
          </ContainerBodyHead>

          <ContainerBodyContent>
            <Table className="body-table">
              <TableHead>
                <TableRow>
                  <TableHeadCell
                    onClick={setSortedBy}
                    name="name"
                    sortedBy={sortedBy}
                  >
                    Nama
                  </TableHeadCell>
                  <TableHeadCell
                    onClick={setSortedBy}
                    name="phone"
                    sortedBy={sortedBy}
                  >
                    Nomor Telepon
                  </TableHeadCell>
                  <TableHeadCell>User ID</TableHeadCell>
                  <TableHeadCell
                    onClick={setSortedBy}
                    name="role"
                    sortedBy={sortedBy}
                  >
                    Jabatan
                  </TableHeadCell>
                </TableRow>
              </TableHead>

              <RenderBody
                data={currentData()}
                onClick={(payload) => dispatch({ type: ADMIN.HANDLE_EDIT, payload })}
              />

              <TableFooter>
                <TableRow>
                  <TableCell colSpan={10}>
                    <div className="row px-3">
                      <div className="col-2 flex-auto" />
                      <div className="col-8 d-flex justify-content-center">
                        <Pagination
                          next={next}
                          prev={prev}
                          jump={jump}
                          pages={pages}
                          currentPage={currentPage}
                          maxPage={maxPage}
                        />
                      </div>
                      <Button
                        className="col-2"
                        theme="primary"
                        onClick={() => dispatch({ type: ADMIN.TOGGLE_IS_OPEN })}
                      >
                        Tambah Admin
                      </Button>
                    </div>
                  </TableCell>
                </TableRow>
              </TableFooter>
            </Table>
          </ContainerBodyContent>
        </Panel>
      </ContainerBody>
    </ParentContainer>
  );
}

const RenderBody = ({
  data,
  onClick,
}: {
  data: IAdmin[];
  onClick: (item: IAdmin) => void;
}) => {
  return (
    <TableBody>
      {!data.length
        ? null
        : data.map((item, i) => {
            return (
              <TableRow key={i} onClick={() => onClick(item)}>
                <TableCell style={{ width: "25%" }}>{item.name}</TableCell>
                <TableCell style={{ width: "20%" }}>{item.phone}</TableCell>
                <TableCell style={{ width: "30%" }}>{item._id}</TableCell>
                <TableCell style={{ width: "20%" }}>{item.role}</TableCell>
              </TableRow>
            );
          })}
    </TableBody>
  );
};
