import { ReactNode, useEffect, useState } from "react";
import {
  Button,
  Form,
  Input,
  Modal,
  ModalBody,
  ModalHeader,
  SubjectTitle,
} from "../../components";
import { IAdmin } from "../../redux/_interface/admin";

interface Props {
  isOpen?: boolean;
  isLoading?: boolean;
  editingData?: IAdmin;
  toggle?: () => void;
  handleSimpan: (item: Object) => void;
  handleDelete?: () => void;
}

export const PopupManageAdmin = ({
  isOpen,
  isLoading,
  editingData,
  toggle,
  handleSimpan,
  handleDelete,
}: Props) => {
  //* LOCAL STATE
  const [name, setName] = useState(editingData?.name || "");
  const [phone, setPhone] = useState(editingData?.phone || "");
  const [role, setRole] = useState(editingData?.role || "");
  const [password, setPassword] = useState(editingData?.password || "");
  const [password2, setPassword2] = useState(editingData?.password || "");

  //* FUNCTION
  const resetState = () => {
    setName("");
    setPhone("");
    setRole("");
    setPassword("");
    setPassword2("");
  };

  function validation() {
    let errMsg = [];
    if (!name) errMsg.push("- Nama harus diisi.");
    if (!phone) errMsg.push("- Nomor HP harus diisi.");
    if (!role) errMsg.push("- Jabatan harus diisi.");
    if (!password) errMsg.push("- Password harus diisi.");
    if (password !== password2) errMsg.push("- Password tidak sesuai.");
    return errMsg;
  }

  const onSimpan = async () => {
    if (!toggle || isLoading) return;
    const errMsg = validation();
    if (errMsg.length) return alert(errMsg.join("\n"));

    try {
      const body = { name, phone, role, password };
      await Promise.all([handleSimpan(body)]);
      resetState();
    } catch (err) {
      console.log("error simpan");
      console.error(err);
    }
  };

  useEffect(() => {
    if (editingData) {
      setName(editingData.name);
      setPhone(editingData.phone);
      setRole(editingData.role);
      setPassword(editingData.password);
      setPassword2(editingData.password);
    } else {
      resetState();
    }
  }, [editingData]);

  return (
    <Modal className="modal__add-admin" size="lg" isOpen={isOpen}>
      <ModalHeader
        title={
          editingData ? `Edit Data ${editingData.name}` : "Pembuatan Admin Baru"
        }
        toggle={() => toggle && toggle()}
      />
      <ModalBody>
        <ContentItem title="Informasi Akun" isRow={false}>
          <Input
            label="Nama User"
            value={name}
            onChange={({ target }) => setName(target.value)}
            placeholder="Nama User"
          />
          <Input
            label="Nomor HP"
            value={phone}
            onChange={({ target }) => setPhone(target.value)}
            placeholder="Nomor HP"
          />
        </ContentItem>

        <ContentItem title="Role Akun">
          <Input
            label="Jabatan"
            value={role}
            onChange={({ target }) => setRole(target.value)}
            type="select"
            containerClassName="col-6"
          >
            <option value="">Pilih Jabatan</option>
            <option value="super">Super</option>
            {/* <option value="sale">Sale</option>
            <option value="product">Product</option> */}
            <option value="courier">Courier</option>
          </Input>
        </ContentItem>

        <ContentItem title="Password">
          <Input
            label="Password"
            value={password}
            onChange={({ target }) => setPassword(target.value)}
            placeholder="Password"
            containerClassName="col-6"
          />
          <Input
            label="Ulangi Password"
            value={password2}
            onChange={({ target }) => setPassword2(target.value)}
            placeholder="Ulangi Password"
            containerClassName="col-6"
          />
        </ContentItem>

        <div className="button-action d-flex justify-content-end">
          <Button
            isLoading={isLoading}
            disabled={isLoading}
            onClick={handleDelete}
            minWidth={80}
            className="mt-3"
            theme="danger"
          >
            Hapus
          </Button>
          &nbsp; &nbsp;
          <Button
            isLoading={isLoading}
            disabled={isLoading}
            onClick={onSimpan}
            minWidth={80}
            className="mt-3"
            theme="primary"
          >
            Simpan
          </Button>
        </div>
      </ModalBody>
    </Modal>
  );
};

const ContentItem = ({
  children,
  title,
  isRow = true,
}: {
  children: ReactNode;
  title: string;
  isRow?: boolean;
}) => {
  return (
    <div className="content-item">
      <div className="header">
        <SubjectTitle bold>{title}</SubjectTitle>
      </div>

      <Form className={`body ${isRow && "row"}`}>{children}</Form>
    </div>
  );
};
