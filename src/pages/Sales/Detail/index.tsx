import moment from "moment";
import { UilImport } from "@iconscout/react-unicons";
import { useSelector } from "react-redux";
import {
  Button,
  ButtonIcon,
  ContainerBody,
  ContainerHead,
  DetailContainer,
} from "../../../components";
import { RootState } from "../../../redux/store";
import { enumTrxStatus, formatCurrency, joinAddress } from "../../../utils";

export default function TransactionDetail() {
  //* GLOBAL STATUS
  const data = useSelector((state: RootState) => state.trx.transaction);

  return (
    <DetailContainer title="Transaction Detail" className="sales detail">
      <ContainerHead backButton title="Detail Transaksi">
        <Button minWidth={150} theme="danger" isRounded>
          Tolak
        </Button>
        <Button minWidth={150} theme="primary" isRounded>
          Konfirmasi
        </Button>
      </ContainerHead>

      <ContainerBody>
        {/* // Detail Transaction Left */}
        <div className="detail-transaction-left">
          <div className="status-trasaction">
            <div className="sales-order">
              <span>Update Status Transaksi</span>
              <div className="status-order">
                <span>{enumTrxStatus(data?.status)}</span>
                &nbsp;-&nbsp;
                <span className="status -paid">Paid</span>
              </div>
            </div>

            <div className="transaction-date">
              <span>{data ? moment(data.updatetAt).format("HH:mm") : ""}</span>
              <span>{data ? moment(data.updatetAt).format("DD/MM/YYYY") : ""}</span>
              <span className="detail-button">Lihat Detail</span>
            </div>
          </div>

          <div className="data-buyer">
            <span className="page-title">Data Buyer</span>

            <div className="divider" />

            <div className="buyer-profile">
              <span className="head-text">Nama Buyer</span>
              <span className="head-child-name">{data?.buyer.fullName || "-"}</span>
            </div>
            <div className="buyer-profile">
              <span className="head-text">Nomor Kontak</span>
              <span className="head-child-contact">
                {data?.buyer.phoneNumber || "-"}
              </span>
            </div>
            <div className="buyer-profile">
              <span className="head-text">Alamat Buyer</span>
              <span className="head-child-adress">{joinAddress(data?.address)}</span>
            </div>
          </div>

          <div className="data-pengiriman">
            <span className="page-title">Data Pengiriman</span>

            <div className="divider" />

            <div className="buyer-profile">
              <span className="head-text">Alamat Pengiriman</span>
              <span className="head-child-adress">{joinAddress(data?.address)}</span>
            </div>
          </div>

          <div className="data-transaksi">
            <span className="page-title">Data Transaksi</span>

            <div className="buyer-profile">
              <span className="head-text">Tanggal Permintaan</span>
              <span className="head-child-request">Selasa, 26/04/2020</span>
              <span className="head-child-request">
                {moment(data?.createdAt).format("dddd, DD/MM/YYYY")}
              </span>
            </div>
            <div className="buyer-profile">
              <span className="head-text">Kode Transaki</span>
              <span className="head-child-transaction">
                {data?.transactionCode || "-"}
              </span>
            </div>
            <div className="buyer-profile">
              <span className="head-text">Performa Invoice</span>
              <div className="download-invoice">
                <span className="head-child-invoice">
                  {data?.invoiceCode || "-"}
                </span>
                <ButtonIcon className="button-download">
                  <UilImport />
                </ButtonIcon>
              </div>
            </div>
          </div>
        </div>

        {/* // Detail Transaction Right */}
        <div className="detail-transaction-right">
          <div className="detail-transaction">
            <span className="page-title">Rincian Penjualan</span>

            {!data || !data.products.length
              ? null
              : data.products.map((item, i) => {
                  return (
                    <div className="detail-sell-wrapp" key={i}>
                      <div className="image-sell">
                        <img
                          src={item.mediaURLs.length ? item.mediaURLs[0] : ""}
                          alt={item.name}
                          className="image"
                        />

                        <div className="product-detail">
                          <span className="product-title">{item.name}</span>
                          <div className="wrapp-flex">
                            <div className="sku">
                              <span className="pruduct-sku">SKU</span>
                              <span className="pruduct-sku">{item.sku}</span>
                            </div>
                            <div className="sku">
                              <span className="pruduct-sku">
                                Kuantitas Pembelian
                              </span>
                              <span className="pruduct-sku">- Kg</span>
                            </div>
                            <div className="sku">
                              <span className="pruduct-sku">Harga Total</span>
                              <span className="pruduct-sku">-</span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  );
                })}

            {/* Tax Section */}
            <div className="total-price">
              <div className="tax-nominal">
                <span>Biaya Tambahan</span>

                <div className="ppn">
                  <span>Pajak PPN</span>
                  <span className="nominal-tax">{formatCurrency(data?.taxFee)}</span>
                </div>
              </div>
              <div className="total-nominal">
                <span className="title-nominal">Total Nominal Transaksi</span>
                <span className="nominal">{formatCurrency(data?.totalPrice)}</span>
              </div>
            </div>
          </div>

          {/* Delivery Status Section */}
          <div className="history-status">
            <span className="page-title">Riwayat Status Ekspedisi</span>

            <div className="kurir-employee">
              <span>Petugas Kurir :</span>
              {/* <span>{data?.courier}</span> */}
            </div>

            {/* <div className="status-expedition">
              <div className="rounded" />

              <div className="status-text">
                <div className="">
                  <span>Dikonfirmasi Kurir</span> - <span>Gudang Empofarm</span>
                </div>
                <span className="secondary-text">
                  Secondary text for additional information or remarks
                </span>
                <span className="third-text">15 Juli 2020, 08:00 WIB</span>
              </div>
            </div> */}
          </div>
        </div>
      </ContainerBody>
    </DetailContainer>
  );
}
