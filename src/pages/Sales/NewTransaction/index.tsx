import React, { useState } from "react";
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  TableHeadCell,
  Panel,
  Form,
  Searchbar,
  Select,
} from "../../../components";

import { DocumentDownload16 } from "@carbon/icons-react";

export default function NewTransaction() {
  const [searchQuery, setSearchQuery] = useState("");
  return (
    <div className="new-transaction">
      <span className="page-title">Laporan Transaksi Penjualan</span>

      <div className="filter-transaction">
        <Form className="col-3">
          <Searchbar
            placeholder="Cari..."
            value={searchQuery}
            onChange={setSearchQuery}
          />
        </Form>

        <div className="transaction-wrapp">
          <div className="download-file">
            <DocumentDownload16 color="#AA00FF" />
            <span>Donwload File Excel</span>
          </div>
          <div className="urutkan-transaksi">
            <span>Urutkan Transaksi</span>
            <Select className="select-transaction">
              <option value="">Transaksi Terbaru</option>
              <option value="">Transaksi Terbaru</option>
              <option value="">Transaksi Terbaru</option>
            </Select>
          </div>
          <div className="semua-transaksi">
            <span>Filter Transaksi</span>
            <Select className="select-transaction">
              <option value="">Semua Transaksi</option>
              <option value="">Belum dikonfirmasi</option>
              <option value="">Dikonfirmasi</option>
              <option value="">Dalam Pengiriman</option>
            </Select>
          </div>
        </div>
      </div>

      <Panel>
        <Table>
          <TableHead>
            <TableRow>
              <TableHeadCell>
                <select className="user-select" name="" id="">
                  <option value="">Tanggal Transaksi</option>
                </select>
              </TableHeadCell>
              <TableHeadCell>
                <select className="user-select" name="" id="">
                  <option value="">Order ID</option>
                </select>
              </TableHeadCell>
              <TableHeadCell>
                <select className="user-select" name="" id="">
                  <option value="">Nama Pembeli</option>
                </select>
              </TableHeadCell>
              <TableHeadCell>
                <select className="user-select" name="" id="">
                  <option value="">Nominal Transaksi</option>
                </select>
              </TableHeadCell>
              <TableHeadCell>
                <select className="user-select" name="" id="">
                  <option value="">Status Transaksi</option>
                </select>
              </TableHeadCell>
            </TableRow>
          </TableHead>

          <TableBody>
            <TableCell>01 May 2021, 00:00</TableCell>
            <TableCell>ORD-1562792773536</TableCell>
            <TableCell>Kairav Sihotang</TableCell>
            <TableCell align="right">Rp.120.000.000</TableCell>
            <TableCell align="right">Selesai</TableCell>
          </TableBody>
        </Table>
      </Panel>
    </div>
  );
}
