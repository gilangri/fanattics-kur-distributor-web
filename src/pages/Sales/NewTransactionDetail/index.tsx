import React, { useState } from "react";
import { ButtonIcon, Panel } from "../../../components";
import { ArrowLeft16, Download16, ProgressBarRound16 } from "@carbon/icons-react";
import { Button } from "../../../components";
import PopUpCancellation from "./Modal/PopUpCancellation";

export default function TransactionDetail() {
  const [PopUpCancellation, setPopUpCancellation] = useState(false);

  const togglePopUpCancellation = () => setPopUpCancellation(!PopUpCancellation);

  return (
    <div className="new-transaction-detail">
      <div className="back-button">
        <ButtonIcon className="button">
          <ArrowLeft16 className="arrow-left" />
        </ButtonIcon>
        <span>Kembali</span>
      </div>

      <div className="breadcrumb">
        <span>Finance / Sales / Transaksi Baru / Transaction Detail</span>
      </div>

      <div className="button-confirmation">
        <h3>Detail Transaksi</h3>

        <div className="button-transaction">
          <Button className="button-cancel">Tolak</Button>
          <Button className="button-confirmation">Konfirmasi</Button>
        </div>
      </div>

      <div className="data-transaction">
        {/* // Detail Transaction Left */}
        <div className="detail-transaction-left">
          <div className="status-trasaction">
            <div className="sales-order">
              <span>Update Status Transaksi</span>
              <div className="status-order">
                <span>Sales Order Request </span>
                <span className="status">- Paid </span>
              </div>
            </div>

            <div className="transaction-date">
              <span>15 : 15</span>
              <span>06/07/2020</span>
              <span className="detail-button">Lihat Detail</span>
            </div>
          </div>

          <div className="data-buyer">
            <span className="page-title">Data Buyer</span>

            <div className="divider" />

            <div className="buyer-profile">
              <span className="head-text">Nama Buyer</span>
              <span className="head-child-name">Aryasatya Bimo</span>
            </div>
            <div className="buyer-profile">
              <span className="head-text">Nomor Kontak</span>
              <span className="head-child-contact">0812-6768-999</span>
            </div>
            <div className="buyer-profile">
              <span className="head-text">Alamat Buyer</span>
              <span className="head-child-adress">Jl. Raya Abadi No. 368</span>
            </div>
          </div>

          <div className="data-pengiriman">
            <span className="page-title">Data Pengiriman</span>

            <div className="divider" />

            <div className="buyer-profile">
              <span className="head-text">Alamat Pengiriman</span>
              <span className="head-child-adress">
                Jl. Wolter Mongonsidi No. 82 (Petogogan, Kebayoran Baru) Jakarta
                Selatan , Jakarta
              </span>
            </div>
          </div>

          <div className="data-transaksi">
            <span className="page-title">Data Transaksi</span>

            <div className="buyer-profile">
              <span className="head-text">Tanggal Permintaan</span>
              <span className="head-child-request">Selasa, 26/04/2020</span>
            </div>
            <div className="buyer-profile">
              <span className="head-text">Kode Transaki</span>
              <span className="head-child-transaction">ALEPHOENTR975</span>
            </div>
            <div className="buyer-profile">
              <span className="head-text">Performa Invoice</span>
              <div className="download-invoice">
                <span className="head-child-invoice">INV-10-10010</span>
                <ButtonIcon className="button-download">
                  <Download16 />
                </ButtonIcon>
              </div>
            </div>
          </div>
        </div>

        {/* // Detail Transaction Right */}
        <div className="detail-transaction-right">
          <div className="detail-transaction">
            <span className="page-title">Rincian Penjualan</span>

            <div className="detail-sell-wrapp">
              <div className="image-sell">
                <img
                  src="https://www.static-src.com/wcsstore/Indraprastha/images/catalog/full//97/MTA-3540993/beng-beng_beng--beng-coklat--1--karton-isi-8-box_full02.jpg"
                  alt=""
                  className="image"
                />

                <div className="product-detail">
                  <span className="product-title">Beng Beng</span>
                  <div className="wrapp-flex">
                    <div className="sku">
                      <span className="pruduct-sku">SKU</span>
                      <span className="pruduct-sku">1234567890123456</span>
                    </div>
                    <div className="sku">
                      <span className="pruduct-sku">Kuantitas Pembelian</span>
                      <span className="pruduct-sku">20 Kg</span>
                    </div>
                    <div className="sku">
                      <span className="pruduct-sku">Harga Total</span>
                      <span className="pruduct-sku">Rp 240.000.000</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            {/* Pembatas */}
            <div className="detail-sell-wrapp">
              <div className="image-sell">
                <img
                  src="https://www.static-src.com/wcsstore/Indraprastha/images/catalog/full/MTA-1459488/oreo_oreo-vanilla--133-g-2-pcs-_full05.jpg"
                  alt=""
                  className="image"
                />

                <div className="product-detail">
                  <span className="product-title">Oreo</span>
                  <div className="wrapp-flex">
                    <div className="sku">
                      <span className="pruduct-sku">SKU</span>
                      <span className="pruduct-sku">1234567890123456</span>
                    </div>
                    <div className="quantity">
                      <span className="pruduct-quantity">Kuantitas Pembelian</span>
                      <span className="pruduct-quantity">20 Kg</span>
                    </div>
                    <div className="price">
                      <span className="pruduct-price">Harga Total</span>
                      <span className="pruduct-price">Rp 240.000.000</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            {/* Tax Section */}
            <div className="total-price">
              <div className="tax-nominal">
                <span>Biaya Tambahan</span>

                <div className="ppn">
                  <span>Pajak PPN</span>
                  <span className="nominal-tax">Rp 80.000.000</span>
                </div>
              </div>
              <div className="total-nominal">
                <span className="title-nominal">Total Nominal Transaksi</span>
                <span className="nominal">Rp1.352.500.000</span>
              </div>
            </div>
          </div>

          {/* Delivery Status Section */}
          <div className="history-status">
            <span className="page-title">Rincian Penjualan</span>

            <div className="kurir-employee">
              <span>Petugas Kurir :</span>
              <span> Heo Joon Jae</span>
            </div>

            <div className="status-expedition">
              <div className="rounded" />

              <div className="status-text">
                <div className="">
                  <span>Dikonfirmasi Kurir</span> - <span>Gudang Empofarm</span>
                </div>
                <span className="secondary-text">
                  Secondary text for additional information or remarks
                </span>
                <span className="third-text">15 Juli 2020, 08:00 WIB</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
