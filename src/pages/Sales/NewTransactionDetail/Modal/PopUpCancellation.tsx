/* eslint-disable @typescript-eslint/no-unused-vars */
import React from "react";
import { MdClose } from "react-icons/md";
import {
  BodyText,
  Button,
  Modal,
  ModalBody,
  PaneHeader,
} from "../../../../components";

interface Props {
  isOpen: boolean;
  onCancel?: () => void;
  onSubmit?: () => void;
  toggle?: () => void;
}

export default ({ isOpen, onCancel, onSubmit, toggle }: Props) => {
  return (
    <Modal size="lg" isOpen={isOpen}>
      <ModalBody>
        <h2>daa</h2>
      </ModalBody>
    </Modal>
  );
};
