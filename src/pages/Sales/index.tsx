import moment from "moment";
import { UilFileDownloadAlt } from "@iconscout/react-unicons";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import {
  Button,
  ButtonIcon,
  ContainerBody,
  ContainerHead,
  Form,
  Panel,
  ParentContainer,
  Searchbar,
  Select,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeadCell,
  TableRow,
} from "../../components";
import { RootState } from "../../redux/store";
import { getTransaction } from "../../redux/transaction/actions";
import { TRX_SET, TRX_SET_TRANSACTION } from "../../redux/transaction/types";
import { ITransaction } from "../../redux/_interface";
import { enumTrxStatus, formatCurrency } from "../../utils";

export default function Transaction() {
  const dispatch = useDispatch();

  //* GLOBAL STATE
  const { transactions, filtered } = useSelector((state: RootState) => state.trx);

  //* LOCAL STATE
  const [searchQuery, setSearchQuery] = useState("");

  //* FUNCTION
  const handleSubmitSearch = (e: React.KeyboardEvent<HTMLInputElement>) => {
    if (e.key === "Enter" && !searchQuery) return e.preventDefault();
    // if (e.key === "Enter") {
    //   e.preventDefault();
    //   e.stopPropagation();
    //   dispatch({ type: PRODUCT_SET, payload: { searchQuery: searchQuery } });
    //   dispatch(searchProduct(searchQuery));
    //   // alert(query);
    // }
  };

  //* FETCH DATA
  useEffect(() => {
    dispatch(getTransaction());
  }, [dispatch]);

  useEffect(() => {
    function setFiltered(filtered: ITransaction[]) {
      dispatch({ type: TRX_SET, payload: { filtered } });
    }
    if (!searchQuery) {
      setFiltered(transactions);
    } else {
      const newData = transactions.filter((value) => {
        const query = searchQuery.toLowerCase();
        const fullName = value.buyer.fullName.toLowerCase();
        const orderId = value.transactionCode
          ? value.transactionCode.toLowerCase()
          : "";
        return fullName.includes(query) || orderId.includes(query);
      });
      setFiltered(newData);
    }
  }, [dispatch, searchQuery, transactions]);

  console.log(filtered);
  return (
    <ParentContainer title="Daftar Transaksi" className="sales">
      <ContainerHead title="Daftar Transaksi">
        <Button theme="secondary">
          <ButtonIcon type="prepend">
            <UilFileDownloadAlt color="#0F90F1" />
          </ButtonIcon>
          Download File Excel
        </Button>
      </ContainerHead>

      <ContainerBody>
        <Panel className="body">
          <div className="filter-transaction">
            <Form className="col-3">
              <Searchbar
                placeholder="Cari..."
                value={searchQuery}
                onChange={setSearchQuery}
                onKeyDown={handleSubmitSearch}
              />
            </Form>

            <div className="transaction-wrapp">
              {/* <div className="download-file">
                <DocumentDownload16 color="#AA00FF" />
                <span>Donwload File Excel</span>
              </div> */}

              {/* <div className="urutkan-transaksi">
                <span>Urutkan Transaksi</span>
                <Select className="select-transaction">
                  <option value="">Transaksi Terbaru</option>
                  <option value="">Transaksi Terbaru</option>
                  <option value="">Transaksi Terbaru</option>
                </Select>
              </div> */}

              <div className="semua-transaksi">
                <span>Filter Transaksi</span>
                <Select className="select-transaction">
                  <option value="">Semua Transaksi</option>
                  <option value="">Belum dikonfirmasi</option>
                  <option value="">Dikonfirmasi</option>
                  <option value="">Dalam Pengiriman</option>
                </Select>
              </div>
            </div>
          </div>

          <Table>
            <TableHead>
              <TableRow>
                <TableHeadCell>Tanggal Transaksi</TableHeadCell>
                <TableHeadCell>Nama Pembeli</TableHeadCell>
                <TableHeadCell>Order ID</TableHeadCell>
                <TableHeadCell>Nominal Transaksi</TableHeadCell>
                <TableHeadCell>Status Transaksi</TableHeadCell>
              </TableRow>
            </TableHead>

            <RenderBody data={filtered} />
          </Table>
        </Panel>
      </ContainerBody>
    </ParentContainer>
  );
}

const RenderBody = ({ data }: { data: ITransaction[] }) => {
  const dispatch = useDispatch();
  const history = useHistory();

  const handleGoToDetail = (payload: ITransaction) => {
    dispatch({ type: TRX_SET_TRANSACTION, payload });
    history.push(`/transaction/detail`);
  };

  return (
    <TableBody>
      {!data.length
        ? null
        : data.map((item, i) => {
            return (
              <TableRow key={i} onClick={() => handleGoToDetail(item)}>
                <TableCell>{moment(item.createdAt).format("DD MMM YYYY")}</TableCell>
                <TableCell>{item.buyer.fullName}</TableCell>
                <TableCell>{item.transactionCode}</TableCell>
                <TableCell>{formatCurrency(item.totalPrice)}</TableCell>
                <TableCell>{enumTrxStatus(item.status)}</TableCell>
              </TableRow>
            );
          })}
    </TableBody>
  );
};
