import { useEffect, useState } from "react";
import {
  Tabs,
  Button,
  Searchbar,
  Form,
  ParentContainer,
  ContainerHead,
  ContainerBody,
} from "../../components";
import { Settings16 } from "@carbon/icons-react";
import { useDispatch, useSelector } from "react-redux";
import {
  clearSearch,
  createCategory,
  getCategories,
  getProducts,
  searchProduct,
} from "../../redux/product/actions";
import { RootState } from "../../redux/store";
import { useHistory } from "react-router";
import { ProductCard } from "./_productCard";
import { ModalAddCategory } from "./_modalAddCategory";
import { PRODUCT_SET } from "../../redux/product/types";
import { IProduct } from "../../redux/_interface/product";

export default function ProductCatalog() {
  const dispatch = useDispatch();
  const history = useHistory();

  //* GLOBAL STATE
  const { categories, data, searchQuery, isSearching, searched } = useSelector(
    (state: RootState) => state.product
  );

  //* LOCAL STATE
  const [isAddingCategory, setIsAddingCategory] = useState(false);
  const [activeTab, setActiveTab] = useState("");

  //* VALUE CHANGER
  const toggleAddingCategory = () => setIsAddingCategory(!isAddingCategory);
  const setSearchQuery = (val: string) =>
    dispatch({ type: PRODUCT_SET, payload: { searchQuery: val } });

  useEffect(() => {
    dispatch(getCategories());
  }, [dispatch]);

  useEffect(() => {
    dispatch(getProducts({ category: activeTab }));
  }, [activeTab, dispatch]);

  useEffect(() => {
    if (searchQuery) {
      dispatch({ type: PRODUCT_SET, payload: { isSearching: true } });
    } else {
      dispatch({ type: PRODUCT_SET, payload: { isSearching: false, searched: [] } });
    }
  }, [dispatch, searchQuery]);

  //* FUNCTION
  const goToPengaturanKategori = () => history.push("/product/categories");
  const goToTambahProduk = () => history.push("/product/add-product");
  const goToDetail = (value: IProduct) => {
    dispatch({ type: PRODUCT_SET, payload: { selected: value } });
    history.push("/product/detail");
  };

  const handleAddCategory = async (name: string) => {
    try {
      await Promise.all([dispatch(createCategory(name))]);
      setIsAddingCategory(false);
    } catch (err) {
      console.error(err);
    }
  };

  const handleSubmitSearch = (e: React.KeyboardEvent<HTMLInputElement>) => {
    if (e.key === "Enter" && !searchQuery) return e.preventDefault();
    if (e.key === "Enter") {
      e.preventDefault();
      e.stopPropagation();
      dispatch({ type: PRODUCT_SET, payload: { searchQuery: searchQuery } });
      dispatch(searchProduct(searchQuery));
      // alert(query);
    }
  };

  // console.log(searched);

  return (
    <ParentContainer title="Katalog Produk" className="product-catalogue">
      <ModalAddCategory
        closeButton
        isOpen={isAddingCategory}
        toggle={toggleAddingCategory}
        handleSimpan={handleAddCategory}
      />
      <ContainerHead title="Katalog Produk" />

      <ContainerHead className="header-action">
        <Form className="search-bar">
          <Searchbar
            placeholder="Cari Produk"
            value={searchQuery}
            onChange={setSearchQuery}
            onKeyDown={handleSubmitSearch}
          />
        </Form>

        <Button theme="primary" onClick={toggleAddingCategory}>
          Buat Kategori
        </Button>
        <Button theme="primary" onClick={goToTambahProduk}>
          Tambah Produk
        </Button>
        <Button theme="secondary" onClick={goToPengaturanKategori}>
          Pengaturan Kategori &nbsp;
          <Settings16 />
        </Button>
      </ContainerHead>

      <ContainerBody>
        <div className="segment-control">
          <Tabs
            options={["Semua", ...categories.map((val) => val.name)]}
            slug={["", ...categories.map((val) => val._id)]}
            className="tab-items"
            activeTab={activeTab}
            setActiveTab={(val) => setActiveTab(val)}
          />
        </div>

        <div className="catalog-container">
          <div className="card-container">
            {isSearching
              ? !searched.length
                ? null
                : searched.map((item, i) => {
                    return (
                      <ProductCard
                        item={item}
                        onClick={() => goToDetail(item)}
                        key={i}
                      />
                    );
                  })
              : !data.length
              ? null
              : data.map((item, i) => {
                  return (
                    <ProductCard
                      item={item}
                      onClick={() => goToDetail(item)}
                      key={i}
                    />
                  );
                })}
          </div>
        </div>
      </ContainerBody>
    </ParentContainer>
  );
}
