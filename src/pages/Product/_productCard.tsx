import { IProduct } from "../../redux/_interface/product";
import { formatCurrency } from "../../utils";
import ImgPlaceholder from "../../assets/images/img-placeholder-portrait.png";

export const ProductCard = ({
  item,
  onClick,
}: {
  item: IProduct;
  onClick?: () => void;
}) => {
  return (
    <div className="card-catalogue" onClick={onClick}>
      <div className="image-catalogue">
        <img
          src={item.mediaURLs.length ? item.mediaURLs[0] : ImgPlaceholder}
          alt="product"
        />
      </div>

      <span className="product-name">{item.name}</span>

      <div className="status">
        <span>Status:</span>
        <span className={item.isAvailable ? "available" : "unavailable"}>
          {item.isAvailable ? "Available" : "Unavailable"}
        </span>
      </div>

      <div className="price">
        <span>Harga:</span>
        <span className="price-catalogue">{formatCurrency(item.price)}/unit</span>
      </div>

      <div className="sku">
        <span>{item.sku}</span>
      </div>
    </div>
  );
};
