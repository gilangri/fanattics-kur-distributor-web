import { useState } from "react";
import { useSelector } from "react-redux";
import {
  Button,
  Form,
  Input,
  Modal,
  ModalBody,
  ModalHeader,
} from "../../components";
import { RootState } from "../../redux/store";

interface Props {
  isOpen?: boolean;
  toggle?: () => void;
  closeButton?: boolean;
  handleSimpan: (name: string) => void;
}

export const ModalAddCategory = ({
  isOpen,
  toggle,
  closeButton,
  handleSimpan,
}: Props) => {
  //* GLOBAL STATE
  const isLoading = useSelector((state: RootState) => state.product.loading);

  //* LOCAL STATE
  const [categoryName, setCategoryName] = useState("");

  //* FUNCTION
  const onSimpan = async () => {
    if (isLoading) return;
    try {
      await Promise.all([handleSimpan(categoryName)]);
      setCategoryName("");
    } catch (err) {
      console.error(err);
    }
  };

  return (
    <Modal isOpen={isOpen} toggle={!closeButton ? toggle : undefined}>
      <ModalHeader title="Nama Kategori" toggle={closeButton ? toggle : undefined} />
      <ModalBody>
        <Form>
          <Input
            value={categoryName}
            onChange={({ target }) => setCategoryName(target.value)}
            placeholder="Nama Kategori"
            onKeyPress={(e) => e.key === "Enter" && e.preventDefault()}
          />
        </Form>

        <Button
          isLoading={isLoading}
          disabled={isLoading || !categoryName}
          onClick={onSimpan}
          minWidth={80}
          className="mt-3"
          theme="primary"
        >
          Simpan
        </Button>
      </ModalBody>
    </Modal>
  );
};
