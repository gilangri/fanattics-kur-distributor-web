import { useCallback } from "react";
import { useDropzone } from "react-dropzone";
import { MdClose } from "react-icons/md";
import { Button } from "../../../components";

export default function ProductImages({
  defaultValue,
  alt,
  files,
  handleDelete,
  onChange,
}: {
  alt?: string;
  defaultValue?: string;
  files?: File;
  handleDelete: (lastModified: number) => void;
  onChange: (value: File[]) => void;
}) {
  const { getRootProps, getInputProps } = useDropzone({
    multiple: false,
    accept: [".jpg", ".png", ".jpeg"],
    noKeyboard: true,
    onDrop: useCallback((acceptedFiles: File[]) => onChange(acceptedFiles), [
      onChange,
    ]),
  });

  return (
    <div className="product product-images">
      <div {...getRootProps({ className: "dropzone" })}>
        <div className="img-container">
          {files ? (
            <div className="img-catalog">
              <Button
                theme="text"
                onClick={() => files && handleDelete(files.lastModified)}
              >
                <MdClose size={16} />
              </Button>
              <img src={URL.createObjectURL(files)} alt="product" />
            </div>
          ) : (
            <img src={defaultValue} alt={alt} />
          )}
        </div>
      </div>

      <div className="action">
        <input {...getInputProps({ id: "add-product" })} />
        <label htmlFor="add-product">Ganti gambar produk</label>
      </div>
    </div>
  );
}
