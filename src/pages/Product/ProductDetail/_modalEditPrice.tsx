import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import {
  Button,
  Form,
  Input,
  Modal,
  ModalBody,
  ModalHeader,
} from "../../../components";
import { RootState } from "../../../redux/store";
import { formatCurrency, parseLocaleNumber } from "../../../utils";

interface Props {
  isOpen?: boolean;
  toggle?: () => void;
  closeButton?: boolean;
  handleSimpan?: (name: string) => void;
  defaultValue: string;
}

export const ModalEditPrice = ({
  isOpen,
  toggle,
  closeButton,
  handleSimpan,
  defaultValue,
}: Props) => {
  //* GLOBAL STATE
  const isLoading = useSelector((state: RootState) => state.product.loading);

  //* LOCAL STATE
  const [price, setPrice] = useState("");

  //* FUNCTION
  const onSimpan = async () => {
    if (!handleSimpan || isLoading || !defaultValue) return;
    try {
      await Promise.all([handleSimpan(price)]);
      setPrice("");
    } catch (err) {
      console.error(err);
    }
  };

  const handleChangePrice = (val: string) => {
    const parsed = parseLocaleNumber(val.replace("Rp", ""));
    parsed ? setPrice(formatCurrency(parsed)) : setPrice("");
  };

  const onCancel = () => {
    if (!toggle) return;
    setPrice("");
    toggle();
  };

  useEffect(() => {
    setPrice(defaultValue);
  }, [defaultValue]);

  return (
    <Modal isOpen={isOpen} toggle={!closeButton ? toggle : undefined}>
      <ModalHeader title="Ganti Harga" toggle={closeButton ? toggle : undefined} />
      <ModalBody className="popup-category">
        {/* <div className="title">
          Ganti nama kategori <span>{selectedProduct?.name || ""}</span>
        </div> */}

        <Form className="input-category">
          <Input
            value={price}
            onChange={({ target }) => handleChangePrice(target.value)}
            onKeyPress={(e) => e.key === "Enter" && e.preventDefault()}
            // placeholder="Nama Kategori Baru"
          />
        </Form>

        <div className="button-action">
          <Button
            disabled={isLoading}
            onClick={onCancel}
            minWidth={150}
            className="mt-3"
            theme="secondary"
          >
            Batal
          </Button>
          <Button
            isLoading={isLoading}
            disabled={isLoading}
            onClick={onSimpan}
            minWidth={150}
            className="mt-3"
            theme="primary"
          >
            Simpan
          </Button>
        </div>
      </ModalBody>
    </Modal>
  );
};
