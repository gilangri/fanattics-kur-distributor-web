import { useSelector } from "react-redux";
import { Button, Modal, ModalBody, ModalHeader } from "../../../components";
import { RootState } from "../../../redux/store";

interface Props {
  isOpen?: boolean;
  toggle?: () => void;
  closeButton?: boolean;
  handleSimpan?: (name: string) => void;
}

export const ModalDeleteProduct = ({
  isOpen,
  toggle,
  closeButton,
  handleSimpan,
}: Props) => {
  //* GLOBAL STATE
  const isLoading = useSelector((state: RootState) => state.product.loading);

  return (
    <Modal isOpen={isOpen} toggle={!closeButton ? toggle : undefined}>
      <ModalHeader
        title="Hapus Product?"
        toggle={closeButton ? toggle : undefined}
      />
      <ModalBody className="popup-category">
        <div className="button-action d-flex mx-auto" style={{ marginTop: 0 }}>
          <Button
            disabled={isLoading}
            onClick={toggle}
            minWidth={150}
            className="mt-3"
            theme="secondary"
          >
            Batal
          </Button>
          <Button
            isLoading={isLoading}
            disabled={isLoading}
            onClick={handleSimpan}
            minWidth={150}
            className="mt-3"
            theme="danger"
          >
            Hapus
          </Button>
        </div>
      </ModalBody>
    </Modal>
  );
};
