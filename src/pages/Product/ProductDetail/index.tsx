import { UilSetting, UilToggleOn, UilToggleOff } from "@iconscout/react-unicons";
import axios from "axios";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import {
  Button,
  ContainerBody,
  ContainerHead,
  DetailContainer,
} from "../../../components";
import { API_URL, optionsFormData } from "../../../constants";
import {
  PRODUCT_FAILED,
  PRODUCT_REQUEST,
  PRODUCT_SET,
} from "../../../redux/product/types";
import { RootState } from "../../../redux/store";
import { formatCurrency, parseLocaleNumber } from "../../../utils";
import { ModalDeleteProduct } from "./_modalDeleteProduct";
import { ModalEditName } from "./_modalEditName";
import { ModalEditPrice } from "./_modalEditPrice";
import ProductImages from "./_productImages";

export default function ProductDetail() {
  const dispatch = useDispatch();
  const history = useHistory();

  //* GLOBAL STATE
  const loading = useSelector((state: RootState) => state.product.loading);
  const product = useSelector((state: RootState) => state.product.selected);

  //* LOCAL STATE
  const [images, setImages] = useState<File | undefined>();
  const [editName, setEditName] = useState("");
  const [editPrice, setEditPrice] = useState("");
  const [editingStatus, setEditingStatus] = useState(false);
  const [editDescription, setEditDescription] = useState("");
  const [isEditingDescription, setIsEditingDescription] = useState(false);
  const [isDeleting, setIsDeleting] = useState(false);

  //* FUNCTION
  const handleAddFile = async (value: File[]) => {
    // setImages(value[0])

    const id = product?._id || "";
    try {
      const formdata = new FormData();
      formdata.append("product", value[0]);
      dispatch({ type: PRODUCT_REQUEST });
      await axios.put(`${API_URL}/products/${id}`, formdata, optionsFormData);
      const { data } = await axios.get(`${API_URL}/products/${id}`);
      dispatch({
        type: PRODUCT_SET,
        payload: { loading: false, selected: data.result },
      });
      setEditDescription("");
    } catch (err) {
      console.error(err);
      dispatch({ type: PRODUCT_FAILED, payload: err });
    }
  };

  //* HANDLE
  const handleEditName = async (val: string) => {
    const id = product?._id || "";
    try {
      const formdata = new FormData();
      formdata.append("name", val);
      dispatch({ type: PRODUCT_REQUEST });
      await axios.put(`${API_URL}/products/${id}`, formdata, optionsFormData);
      const { data } = await axios.get(`${API_URL}/products/${id}`);
      dispatch({
        type: PRODUCT_SET,
        payload: { loading: false, selected: data.result },
      });
      setEditName("");
    } catch (err) {
      console.error(err);
      dispatch({ type: PRODUCT_FAILED, payload: err });
    }
  };

  const handleEditPrice = async (val: string) => {
    const id = product?._id || "";
    const price = parseLocaleNumber(val.replace("Rp", ""));
    try {
      const formdata = new FormData();
      formdata.append("price", price.toString());
      dispatch({ type: PRODUCT_REQUEST });
      await axios.put(`${API_URL}/products/${id}`, formdata, optionsFormData);
      const { data } = await axios.get(`${API_URL}/products/${id}`);
      dispatch({
        type: PRODUCT_SET,
        payload: { loading: false, selected: data.result },
      });
      setEditPrice("");
    } catch (err) {
      console.error(err);
      dispatch({ type: PRODUCT_FAILED, payload: err });
    }
  };

  const toggleStatus = async () => {
    const id = product?._id || "";
    const status = !product?.isAvailable;
    try {
      const formdata = new FormData();
      formdata.append("isAvailable", status.toString());
      dispatch({ type: PRODUCT_REQUEST });
      setEditingStatus(true);
      await axios.put(`${API_URL}/products/${id}`, formdata, optionsFormData);
      const { data } = await axios.get(`${API_URL}/products/${id}`);
      dispatch({
        type: PRODUCT_SET,
        payload: { loading: false, selected: data.result },
      });
    } catch (err) {
      console.error(err);
      dispatch({ type: PRODUCT_FAILED, payload: err });
    } finally {
      setEditingStatus(false);
    }
  };

  const handleEditDescription = async () => {
    const id = product?._id || "";
    try {
      const formdata = new FormData();
      formdata.append("description", editDescription);
      dispatch({ type: PRODUCT_REQUEST });
      await axios.put(`${API_URL}/products/${id}`, formdata, optionsFormData);
      const { data } = await axios.get(`${API_URL}/products/${id}`);
      dispatch({
        type: PRODUCT_SET,
        payload: { loading: false, selected: data.result },
      });
      // setEditDescription("");
      setIsEditingDescription(false);
    } catch (err) {
      console.error(err);
      dispatch({ type: PRODUCT_FAILED, payload: err });
    }
  };

  const handleDelete = async () => {
    const id = product?._id || "";
    try {
      dispatch({ type: PRODUCT_REQUEST });
      await axios.delete(`${API_URL}/products/${id}`);
      const { data } = await axios.get(`${API_URL}/products`);
      dispatch({
        type: PRODUCT_SET,
        payload: { loading: false, data: data.result },
      });
      history.goBack();
    } catch (err) {
      console.error(err);
      dispatch({ type: PRODUCT_FAILED, payload: err });
    }
  };

  return (
    <DetailContainer
      title="Product Detail"
      className="product-detail product-detail__container"
    >
      <ModalDeleteProduct
        isOpen={isDeleting}
        toggle={() => setIsDeleting(false)}
        handleSimpan={handleDelete}
      />

      <ModalEditPrice
        defaultValue={editPrice}
        isOpen={editPrice !== ""}
        toggle={() => setEditPrice("")}
        handleSimpan={handleEditPrice}
      />

      <ModalEditName
        defaultValue={editName}
        isOpen={editName !== ""}
        toggle={() => setEditName("")}
        handleSimpan={handleEditName}
      />

      <ContainerHead title={product?.name || "-"} backButton>
        <Button
          onClick={() => setEditName(product?.name || "")}
          theme="primary"
          containerStyle={{ marginRight: 8 }}
        >
          Edit Nama Produk
        </Button>

        <Button
          onClick={() => setIsDeleting(true)}
          theme="danger"
          containerStyle={{ marginRight: 50 }}
        >
          Hapus Produk
        </Button>
      </ContainerHead>

      <ContainerBody className="product-detail__body">
        <ProductImages
          defaultValue={
            product?.mediaURLs?.length ? product.mediaURLs[0] : undefined
          }
          files={images}
          onChange={handleAddFile}
          handleDelete={() => setImages(undefined)}
        />

        <div className="product product-informasi">
          <div className="header category">
            <div className="title">Kategori Produk</div>
          </div>
          <div className="category-badge">{product?.category?.name || "-"}</div>

          <div className="header description">
            <div className="title">Deskripsi Produk</div>
            <div className="action">
              {isEditingDescription ? (
                <Button
                  onClick={handleEditDescription}
                  isLoading={loading && isEditingDescription}
                  disabled={loading && isEditingDescription}
                  theme="link"
                >
                  Simpan Deskripsi
                </Button>
              ) : (
                <Button
                  onClick={() => {
                    setEditDescription(product?.description || "");
                    setIsEditingDescription(true);
                  }}
                  theme="link"
                >
                  Edit Deskripsi
                </Button>
              )}
            </div>
          </div>
          <div className="body description">
            {isEditingDescription ? (
              <textarea
                value={editDescription}
                onChange={({ target }) => setEditDescription(target.value)}
                name="description"
                id="desc"
              />
            ) : (
              <textarea defaultValue={product?.description} disabled={true} />
            )}
          </div>
        </div>

        <div className="product product-status">
          <ProductInfo className="status" title="Status Produk">
            <Button
              isLoading={loading && editingStatus}
              disabled={loading && editingStatus}
              onClick={toggleStatus}
              theme="icon"
              className={product?.isAvailable ? "available" : "unavailable"}
            >
              {product?.isAvailable ? (
                <React.Fragment>
                  <UilToggleOn />
                  &nbsp;Tersedia
                </React.Fragment>
              ) : (
                <React.Fragment>
                  <UilToggleOff />
                  &nbsp;Tidak Tersedia
                </React.Fragment>
              )}
            </Button>
          </ProductInfo>

          <ProductInfo
            className="sku"
            title="SKU Produk"
            value={product?.sku || "-"}
          />

          <ProductInfo
            className="price"
            title="Harga per Unit"
            value={formatCurrency(product?.price || 0)}
          >
            <Button
              theme="icon"
              onClick={() => setEditPrice(formatCurrency(product?.price || 0))}
            >
              <UilSetting />
            </Button>
          </ProductInfo>
        </div>
      </ContainerBody>
    </DetailContainer>
  );
}

interface IInfo {
  children?: React.ReactNode;
  className?: string;
  title: string;
  value?: string;
}
const ProductInfo = ({ children, className, title, value }: IInfo) => {
  return (
    <div className={`product-info ${className}`}>
      <div className="header">
        <div className="title">{title}</div>
        <div className="action">{children}</div>
      </div>

      <div className="body">
        <div className="content">{value}</div>
      </div>
    </div>
  );
};
