import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import {
  Button,
  Form,
  Input,
  Modal,
  ModalBody,
  ModalHeader,
} from "../../../components";
import { RootState } from "../../../redux/store";

interface Props {
  isOpen?: boolean;
  toggle?: () => void;
  closeButton?: boolean;
  handleSimpan?: (name: string) => void;
  defaultValue: string;
}

export const ModalEditName = ({
  isOpen,
  toggle,
  closeButton,
  handleSimpan,
  defaultValue,
}: Props) => {
  //* GLOBAL STATE
  const isLoading = useSelector((state: RootState) => state.product.loading);

  //* LOCAL STATE
  const [name, setName] = useState("");

  //* FUNCTION
  const onSimpan = async () => {
    if (!handleSimpan || isLoading || !defaultValue) return;
    try {
      await Promise.all([handleSimpan(name)]);
      setName("");
    } catch (err) {
      console.error(err);
    }
  };

  const onCancel = () => {
    if (!toggle) return;
    setName("");
    toggle();
  };

  useEffect(() => {
    setName(defaultValue);
  }, [defaultValue]);

  return (
    <Modal isOpen={isOpen} toggle={!closeButton ? toggle : undefined}>
      <ModalHeader title="Ganti Harga" toggle={closeButton ? toggle : undefined} />
      <ModalBody className="popup-category">
        <div className="title">
          Ganti nama produk <span>{defaultValue}</span>
        </div>

        <Form className="input-category">
          <Input
            value={name}
            onChange={({ target }) => setName(target.value)}
            onKeyPress={(e) => e.key === "Enter" && e.preventDefault()}
          />
        </Form>

        <div className="button-action">
          <Button
            disabled={isLoading}
            onClick={onCancel}
            minWidth={150}
            className="mt-3"
            theme="secondary"
          >
            Batal
          </Button>
          <Button
            isLoading={isLoading}
            disabled={isLoading}
            onClick={onSimpan}
            minWidth={150}
            className="mt-3"
            theme="primary"
          >
            Simpan
          </Button>
        </div>
      </ModalBody>
    </Modal>
  );
};
