/* eslint-disable @typescript-eslint/no-unused-vars */
import { useCallback, useEffect, useState } from "react";
import { useDropzone } from "react-dropzone";
import { MdClose } from "react-icons/md";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import {
  Button,
  ContainerBody,
  ContainerHead,
  DetailContainer,
} from "../../../components";
import { createProduct, getCategories } from "../../../redux/product/actions";
import { TCreateProduct } from "../../../redux/product/types";
import { RootState } from "../../../redux/store";
import { formatCurrency, parseLocaleNumber } from "../../../utils";
import { InputSelect, InputText, InputTextArea } from "./_inputText";

export default function AddNewProduct() {
  const dispatch = useDispatch();
  const history = useHistory();

  //* GLOBAL STATE
  const { categories, loading } = useSelector((state: RootState) => state.product);

  //* LOCAL STATE
  const [images, setImages] = useState<File[]>([]);
  const [category, setCategory] = useState<string>("");
  const [name, setName] = useState("");
  const [sku, setSku] = useState("");
  const [price, setPrice] = useState("");
  const [description, setDescription] = useState("");

  //* FUNCTION
  const handleChangePrice = (val: string) => {
    const parsed = parseLocaleNumber(val.replace("Rp", ""));
    parsed ? setPrice(formatCurrency(parsed)) : setPrice("");
  };

  const handleAddFile = (value: File[]) => setImages([...images, ...value]);

  const handleDeleteFile = (lastModified: number) => {
    if (!images.length) return;
    const newFiles = images.filter((val) => val.lastModified !== lastModified);
    setImages(newFiles);
  };

  const handleSimpan = async () => {
    try {
      const parsedPrice = parseLocaleNumber(price.replace("Rp", ""));

      const body: TCreateProduct = {
        images,
        isAvailable: false,
        category,
        name,
        sku,
        price: parsedPrice.toString(),
        description,
      };

      await Promise.all([dispatch(createProduct(body))]);
      history.goBack();
    } catch (err) {
      console.error(err);
    }
  };

  useEffect(() => {
    dispatch(getCategories());
  }, [dispatch]);

  return (
    <DetailContainer title="Tambah Produk" className="add-product">
      <ContainerHead backButtonText="Kembali ke Katalog Produk" />

      <ContainerBody className="add-product-container">
        <div className="page-title text-center">Tambah Produk</div>

        <div className="divider" />
        <div className="page-title mb-3">Gambar</div>

        <ImageUploader
          files={images}
          onChange={handleAddFile}
          handleDelete={handleDeleteFile}
        />

        <div className="divider" />
        <div className="page-title">Detail</div>

        <div className="input-container">
          <InputSelect
            name="category"
            title="Kategori Produk"
            value={category}
            onChange={(e) => setCategory(e.target.value)}
          >
            <option value="">Pilih salah satu kategori</option>
            {categories.map((val, i) => (
              <option key={i} value={val._id}>
                {val.name}
              </option>
            ))}
          </InputSelect>

          <InputText
            name="name"
            title="Nama Produk"
            placeholder="Masukkan Nama Produk"
            value={name}
            onChange={(e) => setName(e.target.value)}
          />

          <InputText
            name="sku"
            title="SKU Produk"
            placeholder="Masukkan SKU Produk"
            value={sku}
            onChange={(e) => setSku(e.target.value)}
          />

          <InputText
            name="price"
            title="Harga Produk"
            placeholder="Masukkan Harga Produk"
            value={price}
            onChange={(e) => handleChangePrice(e.target.value)}
          />

          <InputTextArea
            name="description"
            title="Deskripsi Produk"
            subtitle="Cantumkan deskripsi lengkap sesuai produk. Panjang deskripsi maksimal
              2000 karakter."
            placeholder="Masukkan Deskripsi Produk"
            maxLength={2000}
            value={description}
            onChange={(e) => setDescription(e.target.value)}
          />
        </div>

        <div className="button-action">
          <Button
            theme="secondary"
            disabled={loading}
            onClick={history.goBack}
            minWidth={152}
          >
            Batalkan
          </Button>
          &nbsp; &nbsp; &nbsp; &nbsp;
          <Button
            theme="primary"
            isLoading={loading}
            disabled={loading}
            onClick={handleSimpan}
            minWidth={152}
          >
            Simpan
          </Button>
        </div>
      </ContainerBody>
    </DetailContainer>
  );
}

const ImageUploader = ({
  files,
  handleDelete,
  onChange,
}: {
  files: File[];
  handleDelete: (lastModified: number) => void;
  onChange: (value: File[]) => void;
}) => {
  const { getRootProps, getInputProps } = useDropzone({
    multiple: true,
    accept: [".jpg", ".png", ".jpeg"],
    noKeyboard: true,
    onDrop: useCallback((acceptedFiles: File[]) => onChange(acceptedFiles), [
      onChange,
    ]),
  });

  return (
    <div className="image-upload-container">
      <div className="image-uploader">
        <div {...getRootProps({ className: "dropzone" })}>
          <div className="image-catalog">
            <label className="component-btn component-btn-primary">Unggah</label>
          </div>
          <input {...getInputProps()} />
        </div>

        {files.map((file, i) => {
          return (
            <div key={i} className="image-catalog">
              <Button theme="text" onClick={() => handleDelete(file.lastModified)}>
                <MdClose size={16} />
              </Button>
              <img src={URL.createObjectURL(file)} alt="product" />
            </div>
          );
        })}
      </div>
    </div>
  );
};
