import { useDispatch, useSelector } from "react-redux";
import { ContainerBody, ContainerHead, DetailContainer } from "../../../components";
import { RootState } from "../../../redux/store";
import { useState } from "react";
import { formatQuantity } from "../../../utils";
import { Dropdown } from "./_dropdown";
import { ModalEditCategory } from "../_modalEditCategory";
import { ICategory } from "../../../redux/_interface/product";
import { deleteCategory, editCategory } from "../../../redux/product/actions";

export default function CategoryConfiguration() {
  const dispatch = useDispatch();

  //* GLOBAL STATE
  const { categories, loading } = useSelector((state: RootState) => state.product);

  //* LOCAL STATE
  const [isOpen, setIsOpen] = useState("");
  const [selectedCategory, setSelectedCategory] = useState<ICategory | undefined>(
    undefined
  );
  const [deletedCategory, setDeletedCategory] = useState<ICategory | undefined>(
    undefined
  );

  //* TOGGLE
  const toggleMenu = (val: string) => {
    isOpen === val ? setIsOpen("") : setIsOpen(val);
  };
  const toggleSelectCategory = (val: ICategory) => {
    if (loading) return;
    setIsOpen("");
    setSelectedCategory(val);
  };
  const toggleUnselectCategory = () => setSelectedCategory(undefined);

  //* FUNCTION
  const handleSimpanEdit = async (value: string) => {
    if (!selectedCategory) return;
    try {
      const body = { ...selectedCategory, name: value };
      await Promise.all([dispatch(editCategory(body))]);
      toggleUnselectCategory();
    } catch (err) {
      console.error(err);
    }
  };

  const handleDelete = async (value: ICategory) => {
    if (loading) return;
    try {
      setDeletedCategory(value);
      await Promise.all([dispatch(deleteCategory(value._id))]);
      setDeletedCategory(undefined);
      setIsOpen("");
    } catch (err) {
      console.error(err);
    }
  };

  return (
    <DetailContainer title="Pengaturan Kategori" className="category-configuration">
      <ModalEditCategory
        selectedCategory={selectedCategory}
        isOpen={selectedCategory !== undefined}
        toggle={toggleUnselectCategory}
        handleSimpan={handleSimpanEdit}
      />

      <ContainerHead
        backButtonText="Kembali ke Katalog Produk"
        title="Pengaturan Kategori"
        titleStyle={{ bold: false }}
      />

      <ContainerBody className="catalog">
        {categories.map((value, i) => (
          <CategoryCard
            key={i}
            name={value.name}
            isOpen={isOpen === value._id}
            isDeleting={deletedCategory?._id === value._id && loading}
            toggle={() => toggleMenu(value._id)}
            handleDelete={() => handleDelete(value)}
            handleEdit={() => toggleSelectCategory(value)}
          />
        ))}

        <CategoryCard name="Produk Tanpa Kategori" />
      </ContainerBody>
    </DetailContainer>
  );
}

const CategoryCard = ({
  name,
  skuLength = 0,
  isOpen,
  isDeleting,
  toggle,
  handleDelete,
  handleEdit,
}: {
  name: string;
  skuLength?: number;
  isOpen?: boolean;
  isDeleting?: boolean;
  toggle?: React.MouseEventHandler<any>;
  handleDelete?: React.MouseEventHandler<any>;
  handleEdit?: React.MouseEventHandler<any>;
}) => {
  return (
    <div className="card-container">
      <div className="card__header">
        <div className="title">Kategori</div>
        {!toggle ? null : (
          <div className="action">
            <Dropdown
              isOpen={isOpen}
              isDeleting={isDeleting}
              toggle={toggle}
              handleDelete={handleDelete}
              handleEdit={handleEdit}
            />
          </div>
        )}
      </div>

      <div className="card__body">
        <div className={`title ${!toggle && "text-danger"}`}>{name}</div>
        <div className="sku-badge">{formatQuantity(skuLength)} SKU</div>
      </div>
    </div>
  );
};
