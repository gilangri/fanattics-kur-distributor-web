import { Dropdown as MyDropdown, DropdownMenu, DropdownToggle } from "reactstrap";
import { Edit16, OverflowMenuVertical16, TrashCan16 } from "@carbon/icons-react";
import { Spinner } from "../../../components";

export const Dropdown = ({
  isOpen,
  isDeleting,
  toggle,
  handleDelete,
  handleEdit,
}: {
  isOpen?: boolean;
  isDeleting?: boolean;
  toggle?: React.MouseEventHandler<any>;
  handleDelete?: React.MouseEventHandler<any>;
  handleEdit?: React.MouseEventHandler<any>;
}) => {
  return (
    <MyDropdown isOpen={isOpen} toggle={toggle} className="dropdown__menu-button">
      <DropdownToggle tag="span">
        <OverflowMenuVertical16 />
      </DropdownToggle>

      <DropdownMenu right>
        <DropdownItem isDisabled>Pengaturan</DropdownItem>
        <DropdownItem name="edit" onClick={handleEdit}>
          Edit Informasi&nbsp;
          <Edit16 />
        </DropdownItem>
        <DropdownItem name="delete" onClick={handleDelete}>
          Hapus Kategori&nbsp;
          {isDeleting ? <Spinner /> : <TrashCan16 />}
        </DropdownItem>
      </DropdownMenu>
    </MyDropdown>
  );
};

const DropdownItem = ({
  isDisabled,
  children,
  name = "",
  onClick,
}: {
  isDisabled?: boolean;
  children?: React.ReactNode;
  name?: string;
  onClick?: React.MouseEventHandler<HTMLDivElement>;
}) => {
  return (
    <div
      onClick={onClick}
      className={`menu-item menu-item-${name} ${isDisabled && "disabled"}`}
    >
      {children}
    </div>
  );
};
