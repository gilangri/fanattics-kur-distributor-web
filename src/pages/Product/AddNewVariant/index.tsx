import React from "react";
import { ArrowLeft16 } from "@carbon/icons-react";
import { ButtonIcon, Button } from "../../../components";
import { Input } from "reactstrap";

export default function AddNewVariant() {
  return (
    <div className="add-new-variant">
      <div className="back-button">
        <ButtonIcon className="button">
          <ArrowLeft16 className="arrow-left" />
        </ButtonIcon>
        <span>Kembali ke Produk Catalog</span>
      </div>

      <div className="add-product-container">
        <span className="page-title">Tambah Produk</span>

        <div className="divider" />

        <span className="page-title">Gambar</span>

        <div className="upload-image">
          <form>
            <input type="file" title="your text" />
          </form>
        </div>

        <div className="divider" />
        <span className="page-title">Deskripsi</span>

        <div className="text-field">
          <span>Nama Produk</span>
          <Input className="col-9" placeholder="Masukkan Nama Produk" />
        </div>
        <div className="text-field">
          <span>SKU Produk</span>
          <Input className="col-9" placeholder="Masukkan SKU secara manual" />
        </div>
        <div className="text-field-description">
          <div className="description-product">
            <span>Deskripsi Produk</span>
            <span className="description-text">
              Include a complete description according to the product. The length of
              the description is between 450-2000 characters.
            </span>
          </div>
          {/* <Input className="col-9" placeholder="Masukkan Deskripsi Produk" /> */}
          <textarea
            className="text-area"
            placeholder="Masukan Deskripsi Produk"
          ></textarea>
        </div>

        <div className="button-container">
          <Button theme="outline-radius" minWidth={152}>
            Batalkan
          </Button>
          &nbsp; &nbsp; &nbsp; &nbsp;
          <Button theme="primary-radius" minWidth={152}>
            Simpan
          </Button>
        </div>
      </div>
    </div>
  );
}
