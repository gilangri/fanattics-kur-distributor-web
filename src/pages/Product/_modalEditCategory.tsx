import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import {
  Button,
  Form,
  Input,
  Modal,
  ModalBody,
  ModalHeader,
} from "../../components";
import { RootState } from "../../redux/store";
import { ICategory } from "../../redux/_interface/product";

interface Props {
  isOpen?: boolean;
  toggle?: () => void;
  closeButton?: boolean;
  handleSimpan?: (name: string) => void;
  selectedCategory?: ICategory;
}

export const ModalEditCategory = ({
  isOpen,
  toggle,
  closeButton,
  handleSimpan,
  selectedCategory,
}: Props) => {
  //* GLOBAL STATE
  const isLoading = useSelector((state: RootState) => state.product.loading);

  //* LOCAL STATE
  const [categoryName, setCategoryName] = useState("");

  //* FUNCTION
  const onSimpan = async () => {
    if (!handleSimpan || isLoading || !selectedCategory) return;
    try {
      await Promise.all([handleSimpan(categoryName)]);
      setCategoryName("");
    } catch (err) {
      console.error(err);
    }
  };

  useEffect(() => {
    if (!selectedCategory) return;
    setCategoryName(selectedCategory.name);
  }, [selectedCategory]);

  return (
    <Modal isOpen={isOpen} toggle={!closeButton ? toggle : undefined}>
      <ModalHeader
        title="Ganti Nama Kategori"
        toggle={closeButton ? toggle : undefined}
      />
      <ModalBody className="popup-category">
        <div className="title">
          Ganti nama kategori <span>{selectedCategory?.name || ""}</span>
        </div>

        <Form className="input-category">
          <Input
            value={categoryName}
            onChange={({ target }) => setCategoryName(target.value)}
            onKeyPress={(e) => e.key === "Enter" && e.preventDefault()}
            placeholder="Nama Kategori Baru"
          />
        </Form>

        <div className="button-action">
          <Button
            disabled={isLoading}
            onClick={toggle}
            minWidth={150}
            className="mt-3"
            theme="secondary"
          >
            Batal
          </Button>
          <Button
            isLoading={isLoading}
            disabled={isLoading}
            onClick={onSimpan}
            minWidth={150}
            className="mt-3"
            theme="primary"
          >
            Simpan
          </Button>
        </div>
      </ModalBody>
    </Modal>
  );
};
