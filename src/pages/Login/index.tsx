/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState, useEffect } from "react";
import { Form, Input, Button } from "../../components";
import { useSelector, useDispatch } from "react-redux";
import KurLogo from "../../assets/images/kur-logo.png";
import { AUTH_LOGIN, AUTH_SET_VALUE } from "../../redux/auth/types";
import { SIDEBAR_ROLE } from "../../redux/sidebar/types";
import { handleLogin } from "../../redux/auth/actions";
import { RootState } from "../../redux/store";
import { Spinner } from "reactstrap";

export default function Login() {
  const dispatch = useDispatch();

  const [loginForm, setLoginForm] = useState({ phone: "", password: "" });

  const auth = useSelector((state: RootState) => state.auth);
  const loading = useSelector((state: RootState) => state.auth.loadingLogin);

  const valueOnchange = (e: any) => {
    let name = e.target.name;
    let value = e.target.value;
    setLoginForm({ ...loginForm, [name]: value });
    dispatch({ type: AUTH_SET_VALUE, payload: { name, value } });
  };

  const submitLogin = () => {
    dispatch(handleLogin({ payload: loginForm }));
  };
  console.log(auth);
  return (
    <div className="login-page">
      <div className="image-wrapper">
        <img
          className="img-illustration"
          src="https://images.pexels.com/photos/6413655/pexels-photo-6413655.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"
          alt="empofarm-bg"
        />
      </div>
      <div className="content-wrapper">
        <div className="logo-wrapper">
          <img className="img-logo" src={KurLogo} alt="empofarm-logo" />
        </div>

        <div className="subtitle">Selamat Datang di Empofarm Dashboard</div>
        <div className="title">Login Ke Akun Anda</div>

        <div className="form-wrapper">
          <Form>
            <Input
              label="Nomor Telepon"
              name="phone"
              onChange={valueOnchange}
              placeholder="Masukkan Nomor Telepon"
              type="text"
            />

            <Input
              label="Password"
              name="password"
              onChange={valueOnchange}
              placeholder="Masukkan Password"
              type="password"
            />
          </Form>

          <div className="row-info-login d-flex flex-row justify-content-between">
            <div className="form-check">
              <input
                className="form-check-input"
                type="checkbox"
                value=""
                id="ingat-login"
              />
              <label className="form-check-label" htmlFor="ingat-login">
                Ingat Saya
              </label>
            </div>

            <a href="#">Forgot Password?</a>
          </div>

          <Button
            onClick={submitLogin}
            theme="outline"
            containerStyle={{ height: 50, width: "100%", borderRadius: 5 }}
          >
            {loading ? <Spinner size="sm" /> : "Login Sekarang"}
          </Button>

          <br />
          <br />
          <ErrorMessage value={auth.error} />
        </div>
      </div>
    </div>
  );
}

const ErrorMessage = ({ value = "" }) => {
  return <div className="">{value}</div>;
};
