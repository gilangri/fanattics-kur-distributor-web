import React, { useState, useEffect } from "react";
import { ButtonIcon, HeaderText, Tabs } from "../../../components";
import { Panel } from "../../../components/Panel";
import { ArrowLeft16 } from "@carbon/icons-react";
import RiwayatTransaksi from "./RiwayatTransaksi";
import DataRegistrasi from "./DataRegistrasi";
import InformasiUmum from "./InformasiUmum/Index";

export default function ProfileUser() {
  const renderBodyContent = (tab: string) => {
    switch (tab) {
      case "Data Registrasi":
        return <DataRegistrasi />;
      case "Riwayat Transaksi":
        return <RiwayatTransaksi />;
      case "Informasi Umum":
        return <InformasiUmum />;

      default:
        return null;
    }
  };

  const [toggleState, setToggleState] = useState("");

  // const toggleTab = () => {};

  return (
    <div className="profile-user">
      <div className="back-button">
        <ButtonIcon theme="secondary">
          <ArrowLeft16 className="arrow-left" />
        </ButtonIcon>
        <span>Kembali ke UMKM User</span>
      </div>

      {/* <HeaderText>Profile User</HeaderText> */}
      <h2>Profile User</h2>

      <div className="profile-container">
        <div className="profile-user-left">
          <div className="profile-image">
            <div className="umkm-wrapp">
              <span>UMKM</span>
            </div>

            <div className="image">
              <img
                src="https://images.pexels.com/photos/6355177/pexels-photo-6355177.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"
                alt="admin"
              />
            </div>

            <div className="umkm-name-wrapp">
              <span>Raditya Dasa Maulana</span>
            </div>
          </div>

          <div className="user">
            <span className="head-text">User ID</span>
            <span className="child-text">12810983019832912</span>
          </div>
          <div className="user">
            <span className="head-text">Nomor Telepon</span>
            <span className="child-text">0812-8768-999</span>
          </div>
          <div className="user">
            <span className="head-text">Email</span>
            <span className="child-text">Usermail@gmail.com</span>
          </div>
          <div className="user">
            <span className="head-text">Alamat</span>
            <span className="child-text">
              Pellentesque maecenas dictum sit gravida. Duis cursus metus, sed
              sollicitudin consequat elit massa sagittis proin.
            </span>
          </div>
        </div>

        <div className="segment-control">
          <Tabs
            // type="full-block"
            options={["Data Registrasi", "Riwayat Transaksi", "Informasi Umum"]}
            activeTab={toggleState}
            setActiveTab={setToggleState}
          />

          <div className="body-control">{renderBodyContent(toggleState)}</div>
        </div>
      </div>
    </div>
  );
}
