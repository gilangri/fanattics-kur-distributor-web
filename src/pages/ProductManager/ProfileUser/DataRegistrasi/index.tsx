import React from "react";

export default function DataRegistrasi() {
  return (
    <div className="data-registrasi">
      <h2>Data Registrasi</h2>

      <div className="user-information">
        <span className="head-text">NIK</span>
        <span className="child-text">3171011708450001</span>
      </div>

      <div className="user-information-flex">
        <div className="">
          <div className="user-information">
            <span className="head-text">Email</span>
            <span className="child-text">aryasatyabimo@gmail.com</span>
          </div>
          <div className="user-information">
            <span className="head-text">Alamat Toko</span>
            <span className="child-text">
              Jalan R.H Aria Surya Kencana VI No. 34A
            </span>
          </div>
        </div>
        <div className="">
          <div className="user-information">
            <span className="head-text">No Telepon /Handphone</span>
            <span className="child-text">(021)-364-756 / 0856-8821-1390</span>
          </div>
          <div className="user-information">
            <span className="head-text">Alamat Toko 2</span>
            <span className="child-text">
              Jalan R.H Aria Surya Kencana VI No. 34A
            </span>
          </div>
        </div>
      </div>

      <div className="user-image">
        <div className="foto-ktp">
          <span className="head-text">Foto Ktp</span>

          <div className="image">
            <img
              src="https://images.pexels.com/photos/6355177/pexels-photo-6355177.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"
              alt="admin"
            />
          </div>
        </div>

        <div className="foto-selfie">
          <span className="head-text">Foto Ktp</span>

          <div className="image">
            <img
              src="https://images.pexels.com/photos/6355177/pexels-photo-6355177.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"
              alt="admin"
            />
          </div>
        </div>
      </div>
    </div>
  );
}
