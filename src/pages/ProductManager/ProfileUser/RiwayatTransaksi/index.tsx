import React from "react";
import {
  Table,
  TableBody,
  TableCell,
  TableRow,
  TableHeadCell,
  PaneHeader,
  Panel,
  TableHead,
} from "../../../../components";

export default function RiwayatTransaksi() {
  return (
    <div>
      <Panel padding="24">
        <Table>
          <TableHead>
            <TableRow>
              <TableHeadCell>Third Party</TableHeadCell>
              <TableHeadCell>Invoice ID</TableHeadCell>
              <TableHeadCell>Nominal Transaksi</TableHeadCell>
              <TableHeadCell>PPn 10%</TableHeadCell>
              <TableHeadCell>Total Transaksi</TableHeadCell>
            </TableRow>
          </TableHead>

          <TableBody>
            <TableCell>Cell String</TableCell>
            <TableCell>Cell String</TableCell>
            <TableCell>Cell String</TableCell>
            <TableCell>Cell String</TableCell>
            <TableCell>Cell String</TableCell>
          </TableBody>
        </Table>
      </Panel>
    </div>
  );
}
