import React, { useState, useEffect } from "react";
import { Panel } from "../../components/Panel";
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  TableHeadCell,
} from "../../components/Table";
import { Select } from "../../components/Select";

import { useSelector } from "react-redux";
import { Searchbar, Form } from "../../components";

export default function ProductManager() {
  // const sidebarRole = useSelector((state: any) => state.sidebar.role);

  const [searchQuery, setSearchQuery] = useState("");
  return (
    <div className="product-manager">
      <div className="label">
        <span>Label</span> / <span>Label</span> / <span>Customer Management</span> /{" "}
        <span>Katalog User</span>
      </div>
      <h2>Daftar UMKM</h2>

      <div className="checkbox">
        <Form className="checkbox-wrapp">
          <span>Kategori User</span>
          <div className="checkbox-container">
            <input type="checkbox" />
            <label> Farmer</label>
          </div>
          <div className="checkbox-container">
            <input type="checkbox" />
            <label> Seler</label>
          </div>
          <div className="checkbox-container">
            <input type="checkbox" />
            <label> Seler</label>
          </div>
        </Form>
        <Form className="col-3">
          <Searchbar
            placeholder="Cari..."
            value={searchQuery}
            onChange={setSearchQuery}
          />
        </Form>
      </div>
      <Panel padding="24">
        <Table>
          <TableHead>
            <TableRow>
              <TableHeadCell>
                <select className="user-select" name="" id="">
                  <option value="">Nama User</option>
                </select>
              </TableHeadCell>
              <TableHeadCell>
                <select className="user-select" name="" id="">
                  <option value="">Nomor Registrasi</option>
                </select>
              </TableHeadCell>
              <TableHeadCell align="left">
                <select className="user-select" name="" id="">
                  <option value="">Nama UMKM</option>
                </select>
              </TableHeadCell>
              <TableHeadCell>
                <select className="user-select" name="" id="">
                  <option value="">Nomor Telepon</option>
                </select>
              </TableHeadCell>
            </TableRow>
          </TableHead>

          <TableBody>
            <TableCell>
              <div className="user-image">
                <div className="image">
                  <img
                    src="https://images.pexels.com/photos/6355177/pexels-photo-6355177.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"
                    alt="admin"
                  />
                </div>
                <span>Ajimat Teddy Megantara M.M.</span>
              </div>
            </TableCell>
            <TableCell>01906912</TableCell>
            <TableCell>Perum Firmansyah Budiman Tbk</TableCell>
            <TableCell>(629) 555-0129</TableCell>
          </TableBody>
        </Table>
      </Panel>
    </div>
  );
}
