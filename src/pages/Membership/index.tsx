import { useEffect, useState } from "react";
import { FaUserCircle } from "react-icons/fa";
import { FiChevronDown, FiChevronUp } from "react-icons/fi";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import {
  ContainerBody,
  ContainerHead,
  Form,
  Panel,
  ParentContainer,
  Searchbar,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeadCell,
  TableRow,
} from "../../components";
import { gray400 } from "../../constants";
import { getMember } from "../../redux/membership/actions";
import { MEMBERSHIP_SET_USER } from "../../redux/membership/types";
import { RootState } from "../../redux/store";
import { IUser } from "../../redux/_interface/user";

export default function Membership() {
  const dispatch = useDispatch();
  const history = useHistory();

  //* GLOBAL STATE
  const { users } = useSelector((state: RootState) => state.membership);

  //* LOCAL STATE
  const [searchQuery, setSearchQuery] = useState("");

  //* NAVIGATE
  const goToDetail = (payload: IUser) => {
    dispatch({ type: MEMBERSHIP_SET_USER, payload });
    history.push(`${history.location.pathname}/detail`);
  };

  //* FETCH DATA
  useEffect(() => {
    dispatch(getMember());
  }, [dispatch]);

  return (
    <ParentContainer title="Membership" className="membership">
      <ContainerHead title="Daftar UMKM" />
      <ContainerBody>
        <div className="header">
          <Form>
            <Searchbar
              placeholder="Cari user"
              value={searchQuery}
              onChange={setSearchQuery}
            />
          </Form>
        </div>

        <Panel className="body">
          <Table className="membership-list">
            <TableHead>
              <TableRow>
                <HeaderCellItem status="up">Nama User</HeaderCellItem>
                <HeaderCellItem>Nomor Registrasi</HeaderCellItem>
                <HeaderCellItem>Nama UMKM</HeaderCellItem>
                <HeaderCellItem>Nomor Telepon</HeaderCellItem>
              </TableRow>
            </TableHead>

            <RenderBody data={users} onClick={goToDetail} />
          </Table>
        </Panel>
      </ContainerBody>
    </ParentContainer>
  );
}

const RenderBody = ({
  data,
  onClick,
}: {
  data: IUser[];
  onClick: (item: IUser) => void;
}) => {
  return (
    <TableBody>
      {!data.length
        ? null
        : data.map((item, i) => {
            return (
              <TableRow key={i} onClick={() => onClick(item)}>
                <TableCell>
                  <div className="user-image">
                    <FaUserCircle color="#00BCF2" size={20} /> &nbsp;
                    <span>{item.fullName}</span>
                  </div>
                </TableCell>
                <TableCell>{item._id}</TableCell>
                <TableCell>{item.storeName}</TableCell>
                <TableCell>{item.phoneNumber}</TableCell>
              </TableRow>
            );
          })}
    </TableBody>
  );
};

const HeaderCellItem = ({
  children,
  status = "default",
}: {
  children: React.ReactNode;
  status?: "up" | "down" | "default";
}) => {
  return (
    <TableHeadCell>
      <div className="th-item">
        <div className="title">{children}</div>
        <div className="status">
          <FiChevronUp color={status === "up" ? "#000" : gray400} />
          <FiChevronDown color={status === "down" ? "#000" : gray400} />
        </div>
      </div>
    </TableHeadCell>
  );
};
