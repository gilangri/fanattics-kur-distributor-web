import React from "react";

interface IItemContent {
  children?: React.ReactNode;
  style?: React.CSSProperties;
  title: string;
  value?: any;
}
export function ItemContent({ children, style, title, value }: IItemContent) {
  return (
    <div className="item-content" style={style}>
      <div className="content-title">{title}</div>
      <div className="content-value">{value ? value : children}</div>
    </div>
  );
}
