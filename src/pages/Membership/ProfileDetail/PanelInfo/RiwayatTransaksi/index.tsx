/* eslint-disable @typescript-eslint/no-unused-vars */
import {
  BodyText,
  Panel,
  Table,
  TableHead,
  TableHeadCell,
  TableRow,
} from "../../../../../components";

export default function RiwayatTransaksi() {
  return (
    <Panel padding="16" className="details-panel-info riwayat-transaksi">
      {/* <BodyText bold className="mb-2">
        Transaksi
      </BodyText> */}

      <Table>
        <TableHead>
          <TableRow>
            <TableHeadCell>Third Party</TableHeadCell>
            <TableHeadCell>Invoice ID</TableHeadCell>
            <TableHeadCell>Nominal Transaksi</TableHeadCell>
            <TableHeadCell>PPn 10%</TableHeadCell>
            <TableHeadCell>Total Transaksi</TableHeadCell>
          </TableRow>
        </TableHead>
      </Table>
    </Panel>
  );
}
