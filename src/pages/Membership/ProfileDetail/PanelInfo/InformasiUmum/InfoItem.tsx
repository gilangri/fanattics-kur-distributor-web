import React from "react";

interface Props {
  className?: string;
  icon: any;
  title: string;
  value: any;
}
export default function InfoItem(props: Props) {
  const { className, icon, title, value } = props;

  return (
    <div className={`item-container ${className}`}>
      <div className="icon-wrapper">{icon}</div>
      <div className="item-content">
        <div className="title">{title}</div>
        <div className="value">{value}</div>
      </div>
    </div>
  );
}
