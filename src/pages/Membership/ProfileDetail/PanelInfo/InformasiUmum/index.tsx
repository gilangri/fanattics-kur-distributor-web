/* eslint-disable @typescript-eslint/no-unused-vars */
import React from "react";
import moment from "moment";
import { MdDevices } from "react-icons/md";
import { AiOutlineClockCircle } from "react-icons/ai";
import InfoItem from "./InfoItem";
import { Panel } from "../../../../../components";

export default function InformasiUmum(props: any) {
  return (
    <Panel className="details-panel-info informasi-umum">
      <div className="d-flex my-3">
        <InfoItem
          icon={<MdDevices size={40} />}
          title="Perangkat"
          value="Samsung Galaxy M10"
          className="mr-3"
        />

        <InfoItem
          icon={<AiOutlineClockCircle size={40} />}
          title="Login Terakhir"
          value={moment().format("D MMMM YYYY HH:mm")}
        />
      </div>
    </Panel>
  );
}
