/* eslint-disable @typescript-eslint/no-unused-vars */
import { PaneHeader, Panel } from "../../../../../components";
import { IUser } from "../../../../../redux/_interface/user";
import { joinAddress } from "../../../../../utils";
import { ItemContent } from "../_itemContent";

export default function DataRegistrasi({ data }: { data?: IUser }) {
  return (
    <Panel className="details-panel-info data-registrasi">
      <PaneHeader bold className="mb-4" color="#404040">
        Data Registrasi
      </PaneHeader>

      <div className="d-flex">
        <ItemContent title="NIK" value={data?.ktpNumber} style={{ minWidth: 325 }} />
      </div>

      <div className="d-flex">
        <ItemContent title="Email" value={data?.email} style={{ minWidth: 325 }} />
        <ItemContent title="Nomor Telepon" value={data?.phoneNumber} />
      </div>

      <ItemContent
        title="Alamat"
        value={joinAddress(data?.address)}
        style={{ maxWidth: 325 }}
      />

      <div className="d-flex mt-4">
        <ItemContent title="Foto KTP" style={{ marginRight: 14 }}>
          <div className="img-container ktp">
            <img src={data?.ktpUrl || ""} alt={data?.fullName} />
          </div>
        </ItemContent>

        <ItemContent title="Foto Selfie">
          <div className="img-container profile">
            <img src={data?.profilePicture || ""} alt={data?.fullName} />
          </div>
        </ItemContent>
      </div>
    </Panel>
  );
}
