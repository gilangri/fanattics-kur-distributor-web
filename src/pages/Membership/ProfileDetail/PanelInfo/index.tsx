/* eslint-disable @typescript-eslint/no-unused-vars */
import { Fragment, useState } from "react";
import { Panel, Tabs } from "../../../../components";
import { IUser } from "../../../../redux/_interface/user";
import DataRegistrasi from "./DataRegistrasi";
import InformasiUmum from "./InformasiUmum";
import RiwayatTransaksi from "./RiwayatTransaksi";

const MENU = [
  "Data Registrasi",
  "Riwayat Transaksi",
  // "Informasi Umum"
];

export function PanelInfo({ data }: { data?: IUser }) {
  const [activeTab, setActiveTab] = useState<any>(MENU[0]);

  return (
    <Fragment>
      <Tabs
        type="block"
        options={MENU}
        activeTab={activeTab}
        setActiveTab={setActiveTab}
        className="justify-content-start"
      />

      {activeTab === MENU[0] ? (
        <DataRegistrasi data={data} />
      ) : activeTab === MENU[1] ? (
        <RiwayatTransaksi />
      ) : activeTab === MENU[2] ? (
        <InformasiUmum />
      ) : (
        <Panel className="details-panel-info">{activeTab}</Panel>
      )}
    </Fragment>
  );
}
