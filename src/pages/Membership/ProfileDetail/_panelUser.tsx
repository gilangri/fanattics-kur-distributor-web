import { Panel } from "../../../components";
import { IUser } from "../../../redux/_interface/user";
import { joinAddress } from "../../../utils";

export function PanelUser({ data }: { data?: IUser }) {
  return (
    <Panel padding="16" className="detail-panel-user">
      <div className="d-flex flex-column mb-4">
        <div className="user-badge-type">UMKM</div>
        <div className="user-picture">
          <img src={data?.profilePicture || ""} alt={data?.fullName} />
        </div>
        <div className="user-fullname">{data?.fullName}</div>
      </div>

      <UserInfo title="User ID" value={data?._id} />
      <UserInfo title="Nomor Telepon" value={data?.phoneNumber} />
      <UserInfo title="Email" value={data?.email} />
      <UserInfo title="Alamat" value={joinAddress(data?.address)} />
    </Panel>
  );
}

const UserInfo = ({ title, value = "-" }: { title: string; value?: string }) => {
  return (
    <div className="user-info">
      <div className="user-info-title">{title}</div>
      <div className="user-info-value">{value}</div>
    </div>
  );
};
