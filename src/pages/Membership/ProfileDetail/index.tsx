/* eslint-disable @typescript-eslint/no-unused-vars */
import { UilToggleOn, UilToggleOff } from "@iconscout/react-unicons";
import { Fragment } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  Button,
  ContainerBody,
  ContainerHead,
  DetailContainer,
} from "../../../components";
import { changeMemberActivation } from "../../../redux/membership/actions";
import { RootState } from "../../../redux/store";
import { PanelInfo } from "./PanelInfo";
import { PanelUser } from "./_panelUser";

export default function MembershipDetail() {
  const dispatch = useDispatch();

  //* GLOBAL STATE
  const detail = useSelector((state: RootState) => state.membership.user);
  const loading = useSelector((state: RootState) => state.membership.loading);

  //* FUNCTION
  const toggleActivation = async () => {
    if (!detail) return;
    try {
      const formData = new FormData();
      formData.append("active", (!detail.active).toString());
      await Promise.all([dispatch(changeMemberActivation(detail._id, formData))]);
    } catch (err) {
      console.error("failed change activation");
    }
  };

  return (
    <DetailContainer title="Profile UMKM" className="membership">
      <ContainerHead title="Profile UMKM" backButton>
        <Button
          isLoading={loading}
          disabled={loading}
          onClick={toggleActivation}
          theme={detail?.active ? "primary" : "danger"}
          className={detail?.active ? "available" : "unavailable"}
          minWidth={140}
        >
          {detail?.active ? (
            <Fragment>
              {/* <UilToggleOn /> &nbsp; */}
              Active
            </Fragment>
          ) : (
            <Fragment>
              {/* <UilToggleOff /> &nbsp; */}
              Suspended
            </Fragment>
          )}
        </Button>
      </ContainerHead>
      <ContainerBody className="row content">
        <div className="col-3">
          <PanelUser data={detail} />
        </div>

        <div className="col-9">
          <PanelInfo data={detail} />
        </div>
      </ContainerBody>
    </DetailContainer>
  );
}
