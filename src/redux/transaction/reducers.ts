import {
  ITrxAction,
  ITrxState,
  TRX_FAILED,
  TRX_REQUEST,
  TRX_RESET,
  TRX_SET,
  TRX_SET_TRANSACTION,
  TRX_SET_TRANSACTIONS,
  TRX_SET_VALUE,
} from "./types";

const init_state: ITrxState = {
  transactions: [],
  transaction: undefined,

  filtered: [],

  laoding: false,
  error: "",
};

export function transactionReducer(
  state = init_state,
  { payload, name, type }: ITrxAction
): ITrxState {
  switch (type) {
    case TRX_REQUEST:
      return { ...state, laoding: true, error: "" };
    case TRX_FAILED:
      return { ...state, laoding: false, error: payload };

    case TRX_SET_TRANSACTIONS:
      return { ...state, laoding: false, transactions: payload };
    case TRX_SET_TRANSACTION:
      return { ...state, laoding: false, transaction: payload };

    case TRX_SET_VALUE:
      return { ...state, [name]: payload };
    case TRX_SET:
      return { ...state, ...payload };

    case TRX_RESET:
      return init_state;
    default:
      return state;
  }
}
