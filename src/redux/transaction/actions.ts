import axios from "axios";
import { API_URL } from "../../constants";
import { sortArrByDateKey } from "../../utils";
import {
  TRX_FAILED,
  TRX_REQUEST,
  TRX_SET_TRANSACTION,
  TRX_SET_TRANSACTIONS,
} from "./types";

export const getTransaction = (id: string = "") => async (dispatch: any) => {
  try {
    dispatch({ type: TRX_REQUEST });
    if (id) {
      const { data } = await axios.get(`${API_URL}/transactions/${id}`);
      const payload = data.result;
      dispatch({ type: TRX_SET_TRANSACTION, payload });
    } else {
      const { data } = await axios.get(`${API_URL}/transactions`);
      const payload = sortArrByDateKey(data.result, "updatedAt");
      dispatch({ type: TRX_SET_TRANSACTIONS, payload });
    }
  } catch (err) {
    console.error(err);
    dispatch({ type: TRX_FAILED, payload: "GET TRANSACTION FAIL" });
  }
};

export const editTransaction = (id: string, body: Object) => async (
  dispatch: any
) => {
  try {
    dispatch({ type: TRX_REQUEST });
    await axios.put(`${API_URL}/transactions`, body);
    await Promise.all([dispatch(getTransaction(id))]);
  } catch (err) {
    console.error(err);
    dispatch({ type: TRX_FAILED, payload: "GET TRANSACTION FAIL" });
  }
};
