import { AnyAction } from "redux";
import { ITransaction } from "../_interface";

export interface ITrxState {
  transactions: ITransaction[];
  transaction?: ITransaction;

  filtered: ITransaction[];

  laoding: boolean;
  error: string;
}

export const TRX_REQUEST = "TRX_REQUEST";
export const TRX_FAILED = "TRX_FAILED";
export const TRX_RESET = "TRX_RESET";

export const TRX_SET_TRANSACTIONS = "TRX_SET_TRANSACTIONS";
export const TRX_SET_TRANSACTION = "TRX_SET_TRANSACTION";

export const TRX_SET = "TRX_SET";
export const TRX_SET_VALUE = "TRX_SET_VALUE";

export interface ITrxAction extends AnyAction {
  payload: any;
  name: string;
  type:
    | typeof TRX_REQUEST
    | typeof TRX_FAILED
    | typeof TRX_RESET
    //
    | typeof TRX_SET_TRANSACTIONS
    | typeof TRX_SET_TRANSACTION
    //
    | typeof TRX_SET
    | typeof TRX_SET_VALUE;
}
