export interface ISharingPartner {
  _id: string;
  name: string;
  initialCode: string;
  createdAt: string;
  updatetAt: string;
}

export interface IPartner {
  _id: string;
  sharingPartner: ISharingPartner | string;
  name: string;
  initialCode: string;
  revenueSharePercentage: number;
  active: boolean;
  createdAt: string;
  updatetAt: string;
}
