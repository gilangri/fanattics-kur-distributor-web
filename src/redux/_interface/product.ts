export type ICategory = {
  _id: string;
  name: string;
  createdAt: string;
  updatedAt: string;
};

export type IProduct = {
  isAvailable: boolean;
  mediaURLs: string[];
  _id: string;
  name: string;
  sku: string;
  description: string;
  category: ICategory;
  price: number;
  stock: number;
  createdAt: string;
  updatedAt: string;
};
