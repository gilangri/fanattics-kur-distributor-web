export interface IRepresentative {
  _id: string;
  distributor: string;
  firstName: string;
  lastName: string;
  phoneNumber: string;
  email: string;
  jobPosition: string;
  createdAt: string;
  updatetAt: string;
}
