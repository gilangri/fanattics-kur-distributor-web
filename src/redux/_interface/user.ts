import { IAddress } from "./address";

export interface IUser {
  _id: string;
  pinterId: string;
  fullName: string;
  email: string;
  password: string;
  phoneNumber: string;
  ktpNumber: number;
  ktpUrl: string | null;
  profilePicture: string | null;
  storeName: string;
  address?: IAddress | null;
  active: boolean;
  createdAt: string;
  updatedAt: string;
}
