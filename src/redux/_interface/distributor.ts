import { IAddress } from "./address";
import { IBank } from "./bank";
import { IPartner } from "./partner";
import { IRepresentative } from "./representative";

export interface IDistributor {
  _id: string;
  pinterId: string;
  companyPartner: string;
  name: string;
  email: string;
  phoneNumber: string;
  profilePicture: string | null;
  address: IAddress;
  bank: IBank;
  bankAccountNumber: string;
  partners: IPartner[];
  representatives: IRepresentative[];
}
