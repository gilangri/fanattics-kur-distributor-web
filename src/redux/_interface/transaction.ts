import { IAddress } from "./address";
import { IProduct } from "./product";
import { IUser } from "./user";

export interface ITransaction {
  _id: string;
  invoiceCode: string;
  price: number;
  status: string;
  transactionCode: string;
  buyer: IUser;
  address: IAddress | null;
  deliveryStatus: string;
  taxFee: number;
  courier: number;
  qrCode: string;
  vaNumber: string;
  products: IProduct[];
  totalPrice: number;
  createdAt: string;
  updatetAt: string;
}
