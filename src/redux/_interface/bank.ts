export interface IBank {
  _id: string;
  name: string;
  logo: string;
  createdAt: string;
  updatedAt: string;
}
