/**
 * role: "super" | "product" | "sale" | "courier"
 */
export interface IAdmin {
  _id: string;
  name: string;
  phone: string;
  role: string;
  password: string;
  createdAt: string;
  updatedAt: string;
}
