import { IUser } from "./user";

export interface IAddress {
  _id: string;
  user: IUser;
  address: string;
  province: string;
  city: string;
  district: string;
  subdistrict: string;
  postalCode: string;
  default: boolean;
}
