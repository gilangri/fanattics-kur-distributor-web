import { AnyAction } from "redux";
import { IDistributor } from "../_interface/distributor";

export type TCreateDistributor = {
  images: File[];
  fullName: string;
  email: string;
  phoneNumber: string;
  address: string;
  city: string;
  province: string;
  postalCode: string;
  bankName: string;
  bankAccountNumber: string;
  representativeFirstName: string;
  representativeLastName: string;
  representativePhoneNumber: string;
  representativeEmail: string;
  representativeJobPosition: string;
  name: string;
  profilePicture: string;
  // isAvailable: boolean;
};

export interface IDistributorState {
  users: IDistributor[];
  user?: IDistributor;

  filtered: IDistributor[];

  searchQuery: string;
  searched: IDistributor[];
  isSearching: boolean;

  loading: boolean;
  error: string;
}

export const DISTRIBUTOR_REQUEST = "DISTRIBUTOR_REQUEST";
export const DISTRIBUTOR_FAILED = "DISTRIBUTOR_FAILED";
export const DISTRIBUTOR_RESET = "DISTRIBUTOR_RESET";

export const DISTRIBUTOR_SEARCH = "DISTRIBUTOR_SEARCH";
export const DISTRIBUTOR_SEARCH_DONE = "DISTRIBUTOR_SEARCH_DONE";
export const DISTRIBUTOR_SEARCH_RESET = "DISTRIBUTOR_SEARCH_RESET";

export const DISTRIBUTOR_SET_USERS = "DISTRIBUTOR_SET_USERS";
export const DISTRIBUTOR_SET_USER = "DISTRIBUTOR_SET_USER";

export const DISTRIBUTOR_SET = "DISTRIBUTOR_SET";
export const DISTRIBUTOR_SET_VALUE = "DISTRIBUTOR_SET_VALUE";

export interface IDistributorAction extends AnyAction {
  payload: any;
  name: string;
  type:
    | typeof DISTRIBUTOR_REQUEST
    | typeof DISTRIBUTOR_FAILED
    | typeof DISTRIBUTOR_RESET
    //
    | typeof DISTRIBUTOR_SEARCH
    | typeof DISTRIBUTOR_SEARCH_DONE
    | typeof DISTRIBUTOR_SEARCH_RESET
    //
    | typeof DISTRIBUTOR_SET_USER
    | typeof DISTRIBUTOR_SET_USERS
    //
    | typeof DISTRIBUTOR_SET
    | typeof DISTRIBUTOR_SET_VALUE;
}
