import {
  IDistributorAction,
  IDistributorState,
  DISTRIBUTOR_FAILED,
  DISTRIBUTOR_REQUEST,
  DISTRIBUTOR_RESET,
  DISTRIBUTOR_SEARCH,
  DISTRIBUTOR_SEARCH_DONE,
  DISTRIBUTOR_SEARCH_RESET,
  DISTRIBUTOR_SET,
  DISTRIBUTOR_SET_USER,
  DISTRIBUTOR_SET_USERS,
  DISTRIBUTOR_SET_VALUE,
} from "./types";

const init_state: IDistributorState = {
  users: [],
  user: undefined,
  filtered: [],

  searchQuery: "",
  searched: [],
  isSearching: false,

  loading: false,
  error: "",
};

export function profileSettingsReducer(
  state = init_state,
  { payload, name, type }: IDistributorAction
): IDistributorState {
  switch (type) {
    case DISTRIBUTOR_REQUEST:
      return { ...state, loading: true, error: "" };
    case DISTRIBUTOR_FAILED:
      return { ...state, loading: false, error: payload };

    case DISTRIBUTOR_SEARCH:
      return { ...state, isSearching: true };
    case DISTRIBUTOR_SEARCH_DONE:
      return { ...state, searched: payload };
    case DISTRIBUTOR_SEARCH_RESET:
      return { ...state, isSearching: false, searched: [] };

    case DISTRIBUTOR_SET_USERS:
      return { ...state, loading: false, users: payload };
    case DISTRIBUTOR_SET_USER:
      return { ...state, loading: false, user: payload };

    case DISTRIBUTOR_SET:
      return { ...state, ...payload };
    case DISTRIBUTOR_SET_VALUE:
      return { ...state, [name]: payload };

    case DISTRIBUTOR_RESET:
      return init_state;
    default:
      return state;
  }
}
