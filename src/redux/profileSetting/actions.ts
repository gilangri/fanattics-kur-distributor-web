import axios from "axios";
import { API_URL_PINTER, DISTRIBUTOR_ID } from "../../constants";
import {
  DISTRIBUTOR_FAILED,
  DISTRIBUTOR_REQUEST,
  DISTRIBUTOR_SET_USER,
} from "./types";

const id = DISTRIBUTOR_ID;

export const getDistributor = () => async (dispatch: any) => {
  try {
    dispatch({ type: DISTRIBUTOR_REQUEST });

    const { data } = await axios.get(`${API_URL_PINTER}/distributors/${id}`);
    const payload = data.result;
    dispatch({ type: DISTRIBUTOR_SET_USER, payload });
  } catch (err) {
    console.log(err);
    dispatch({ type: DISTRIBUTOR_FAILED, payload: err });
  }
};

export const editDistributor = (body: Object) => async (dispatch: any) => {
  try {
    dispatch({ type: DISTRIBUTOR_REQUEST });
    await axios.put(`${API_URL_PINTER}/distributors/${id}`, body);
    await Promise.all([dispatch(getDistributor())]);
  } catch (err) {
    console.error(err);
    dispatch({ type: DISTRIBUTOR_FAILED, payload: err });
  }
};
