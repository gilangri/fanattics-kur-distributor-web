import Axios from "axios";
import { DecodeToken } from "../../configs/jwt";
import { API_URL } from "../../constants";
import { AUTH_REQUEST, AUTH_LOGIN, AUTH_FAILED, AUTH_RESET } from "./types";

interface Props {
  payload?: any;
}

export const handleLogin = ({ payload }: Props) => {
  return async (dispatch: any) => {
    dispatch({ type: AUTH_REQUEST });
    try {
      if (!payload.phone || !payload.password) {
        let errMsg = "Password / phone tidak boleh kosong";
        return dispatch({ type: AUTH_FAILED, payload: errMsg });
      }
      console.log(payload);
      const { data } = await Axios.post(`${API_URL}/admins/login`, payload);
      dispatch({ type: AUTH_LOGIN, payload: data.result });
      localStorage.setItem("token", JSON.stringify(data.result));
    } catch (err) {
      console.log("err", err.response);
    }
  };
};

export const ReloginAction = (token: any) => {
  return (dispatch: any) => {
    if (token !== undefined) {
      let payload = JSON.parse(token);
      // console.log("payload", payload);
      return dispatch({ type: AUTH_LOGIN, payload });
    } else {
      return dispatch({ type: AUTH_FAILED, payload: "User not authorized" });
    }
  };
};

export const LogoutAction = () => {
  return (dispatch: any) => {
    localStorage.removeItem("token");
    dispatch({ type: AUTH_RESET });
  };
};
