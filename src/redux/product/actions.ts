import axios from "axios";
import { API_URL, optionsFormData } from "../../constants";
import { ICategory, IProduct } from "../_interface/product";
import {
  PRODUCT_FAILED,
  PRODUCT_REQUEST,
  PRODUCT_SET,
  PRODUCT_SET_VALUE,
  TCreateProduct,
} from "./types";

//* CATEGORIES

export const getCategories = (id: string = "") => async (dispatch: any) => {
  try {
    dispatch({ type: PRODUCT_REQUEST });

    if (id) {
      const { data } = await axios.get(`${API_URL}/categories/${id}`);

      dispatch({
        type: PRODUCT_SET,
        payload: { loading: false, category: data.result },
      });
    } else {
      const { data } = await axios.get(`${API_URL}/categories`);

      dispatch({
        type: PRODUCT_SET,
        payload: { loading: false, categories: data.result },
      });
    }
  } catch (err) {
    console.error(err);
    dispatch({ type: PRODUCT_FAILED, payload: err });
  }
};

export const createCategory = (name: string) => async (dispatch: any) => {
  try {
    dispatch({ type: PRODUCT_REQUEST });

    await axios.post(`${API_URL}/categories`, { name });
    await Promise.all([dispatch(getCategories())]);
  } catch (err) {
    console.error(err);
    dispatch({ type: PRODUCT_FAILED, payload: err });
  }
};

export const editCategory = (body: ICategory) => async (dispatch: any) => {
  try {
    dispatch({ type: PRODUCT_REQUEST });

    await axios.put(`${API_URL}/categories/${body._id}`, { name: body.name });
    await Promise.all([dispatch(getCategories())]);
  } catch (err) {
    console.error(err);
    dispatch({ type: PRODUCT_FAILED, payload: err });
  }
};

export const deleteCategory = (id: string) => async (dispatch: any) => {
  try {
    dispatch({ type: PRODUCT_REQUEST });

    await axios.delete(`${API_URL}/categories/${id}`);
    await Promise.all([dispatch(getCategories())]);
  } catch (err) {
    console.error(err);
    dispatch({ type: PRODUCT_FAILED, payload: err });
  }
};

//
//
//* CATEGORIES

export const getProducts = ({
  category,
  id,
}: {
  category?: string;
  id?: string;
}) => async (dispatch: any) => {
  try {
    dispatch({ type: PRODUCT_REQUEST });
    if (id) {
      const { data } = await axios.get(`${API_URL}/products/${id}`);

      dispatch({
        type: PRODUCT_SET,
        payload: { loading: false, selected: data.result },
      });
    } else {
      const { data } = await axios.get(`${API_URL}/products`);

      if (!data.result.length) {
        dispatch({
          type: PRODUCT_SET,
          payload: { loading: false, data: [] },
        });
      } else {
        const result = !category
          ? data.result
          : data.result.filter((val: IProduct) => val.category?._id === category);

        dispatch({
          type: PRODUCT_SET,
          payload: { loading: false, data: result },
        });
      }
    }
  } catch (err) {
    console.error(err);
    dispatch({ type: PRODUCT_FAILED, payload: err });
  }
};

export const createProduct = ({ images, ...body }: TCreateProduct) => async (
  dispatch: any
) => {
  try {
    dispatch({ type: PRODUCT_REQUEST });

    const formdata = new FormData();
    images.forEach((val) => formdata.append("product", val));
    formdata.append("name", body.name);
    formdata.append("sku", body.sku);
    formdata.append("description", body.description);
    formdata.append("isAvailable", body.isAvailable.toString());
    formdata.append("category", body.category);
    formdata.append("price", body.price);
    formdata.append("stock", "0");

    await axios.post(`${API_URL}/products`, formdata, optionsFormData);
    await Promise.all([dispatch(getProducts({}))]);
  } catch (err) {
    console.error(err);
    dispatch({ type: PRODUCT_FAILED, payload: err });
  }
};

export const searchProduct = (payload: string) => async (dispatch: any) => {
  try {
    dispatch({ type: PRODUCT_REQUEST });
    dispatch({ type: PRODUCT_SET_VALUE, name: "searchQuery", payload });
    const { data } = await axios.get(`${API_URL}/products`);

    if (data.result.length) {
      let result = data.result.filter((val: IProduct) => {
        const query = payload.toLowerCase();
        const name = val.name.toLowerCase();
        const sku = val.sku.toLowerCase();
        return name.includes(query) || sku.includes(query);
      });

      dispatch({
        type: PRODUCT_SET,
        payload: { loading: false, searched: result },
      });
    }
  } catch (err) {
    console.error(err);
    dispatch({ type: PRODUCT_FAILED, payload: err });
  }
};

export const clearSearch = () => (dispatch: any) => {
  dispatch({
    type: PRODUCT_SET,
    payload: { loading: false, searchQuery: "", searched: [] },
  });
};
