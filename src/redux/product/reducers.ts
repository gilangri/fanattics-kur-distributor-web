import {
  IProductAction,
  IProductState,
  PRODUCT_FAILED,
  PRODUCT_REQUEST,
  PRODUCT_RESET,
  PRODUCT_SEARCH_DONE,
  PRODUCT_SEARCH_REQUEST,
  PRODUCT_SEARCH_RESET,
  PRODUCT_SET,
  PRODUCT_SET_VALUE,
} from "./types";

const init_state: IProductState = {
  categories: [],
  category: undefined,

  data: [],
  filtered: [],
  selected: undefined,

  searchQuery: "",
  searched: [],
  isSearching: false,

  loading: false,
  error: "",
};

export function productReducer(
  state = init_state,
  { payload, name, type }: IProductAction
): IProductState {
  switch (type) {
    case PRODUCT_REQUEST:
      return { ...state, loading: true, error: "" };
    case PRODUCT_FAILED:
      return { ...state, loading: false, error: payload };

    case PRODUCT_SET:
      return { ...state, ...payload };
    case PRODUCT_SET_VALUE:
      return { ...state, [name]: payload };

    case PRODUCT_SEARCH_REQUEST:
      return { ...state, isSearching: true, searchQuery: payload };
    case PRODUCT_SEARCH_DONE:
      return { ...state, searched: payload };
    case PRODUCT_SEARCH_RESET:
      return { ...state, isSearching: false, searchQuery: "", searched: [] };

    case PRODUCT_RESET:
      return init_state;
    default:
      return state;
  }
}
