import { AnyAction } from "redux";
import { ICategory, IProduct } from "../_interface/product";

export type TCreateProduct = {
  images: File[];
  category: string;
  name: string;
  sku: string;
  price: string;
  description: string;
  isAvailable: boolean;
};

export interface IProductState {
  categories: ICategory[];
  category?: ICategory;

  data: IProduct[];
  filtered: IProduct[];
  selected?: IProduct;

  searchQuery: string;
  searched: IProduct[];
  isSearching: boolean;

  loading: boolean;
  error: string;
}

export const PRODUCT_REQUEST = "PRODUCT_REQUEST";
export const PRODUCT_FAILED = "PRODUCT_FAILED";
export const PRODUCT_RESET = "PRODUCT_RESET";

export const PRODUCT_SEARCH_REQUEST = "PRODUCT_SEARCH_REQUEST";
export const PRODUCT_SEARCH_DONE = "PRODUCT_SEARCH_DONE";
export const PRODUCT_SEARCH_RESET = "PRODUCT_SEARCH_RESET";

export const PRODUCT_SET = "PRODUCT_SET";
export const PRODUCT_SET_VALUE = "PRODUCT_SET_VALUE";

export interface IProductAction extends AnyAction {
  payload: any;
  name: string;
  type:
    | typeof PRODUCT_REQUEST
    | typeof PRODUCT_FAILED
    | typeof PRODUCT_RESET
    //
    | typeof PRODUCT_SEARCH_REQUEST
    | typeof PRODUCT_SEARCH_DONE
    | typeof PRODUCT_SEARCH_RESET
    //
    | typeof PRODUCT_SET
    | typeof PRODUCT_SET_VALUE;
}
