import { AnyAction } from "redux";

export const SIDEBAR_ROLE = "SIDEBAR_ROLE";
export const SIDEBAR_SELECT = "SIDEBAR_SELECT";
export const SIDEBAR_RESET = "SIDEBAR_RESET";

export interface ISidebarState {
  role: string;
  selected: { title: string; slug: string };
}

export interface ISidebarAction extends AnyAction {
  payload?: any;
  type: typeof SIDEBAR_ROLE | typeof SIDEBAR_SELECT | typeof SIDEBAR_RESET;
}
