import {
  SIDEBAR_SELECT,
  SIDEBAR_RESET,
  SIDEBAR_ROLE,
  ISidebarState,
  ISidebarAction,
} from "./types";

const init_state: ISidebarState = {
  role: "",
  selected: { title: "", slug: "" },
};

export function sidebarReducer(
  state = init_state,
  { type, payload }: ISidebarAction
): ISidebarState {
  switch (type) {
    case SIDEBAR_ROLE:
      return { ...state, role: payload };
    case SIDEBAR_SELECT:
      return { ...state, selected: payload };

    case SIDEBAR_RESET:
      return init_state;

    default:
      return state;
  }
}
