import { applyMiddleware, combineReducers, createStore, Store } from "redux";
import thunk from "redux-thunk";
// import { adminReducer } from "./admin/reducers";
import { authReducer } from "./auth/reducers";
import { membershipReducer } from "./membership/reducers";
import { productReducer } from "./product/reducers";
import { sidebarReducer } from "./sidebar/reducers";
import { profileSettingsReducer } from "./profileSetting/reducers";
import { transactionReducer } from "./transaction/reducers";

//
import { adminStateReducer } from "../pages/Admin/state/reducers";
import { distributorReducer } from "../pages/Distributor/state/reducers/distributor";
import { distributorFormReducer } from "../pages/Distributor/state/reducers/distributorForm";

const rootReducer = combineReducers({
  auth: authReducer,
  trx: transactionReducer,
  sidebar: sidebarReducer,
  product: productReducer,
  membership: membershipReducer,
  admin: adminStateReducer,
  distributors: profileSettingsReducer,
  distributor: distributorReducer,
  distributorForm: distributorFormReducer,
});

export type RootState = ReturnType<typeof rootReducer>;

export function configureStore(): Store<RootState> {
  const store = createStore(rootReducer, undefined, applyMiddleware(thunk));
  return store;
}

export * from "./_interface";
