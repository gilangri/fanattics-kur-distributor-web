import axios from "axios";
import { API_URL, optionsFormData } from "../../constants";
import { sortArrByKey } from "../../utils/helpers/sort";
import {
  MEMBERSHIP_FAILED,
  MEMBERSHIP_REQUEST,
  MEMBERSHIP_SET_USER,
  MEMBERSHIP_SET_USERS,
} from "./types";

export const getMember = (id: string = "") => async (dispatch: any) => {
  try {
    dispatch({ type: MEMBERSHIP_REQUEST });

    if (id) {
      const { data } = await axios.get(`${API_URL}/users/${id}`);
      const payload = data.result;
      dispatch({ type: MEMBERSHIP_SET_USER, payload });
    } else {
      const { data } = await axios.get(`${API_URL}/users`);
      const payload = sortArrByKey(data.result, "fullName");
      dispatch({ type: MEMBERSHIP_SET_USERS, payload });
    }
  } catch (err) {
    console.error(err);
    dispatch({ type: MEMBERSHIP_FAILED, payload: err });
  }
};

/**
 * Change member status activation
 * @param id editing user _id
 * @param formData formData consist of "active"
 * @returns nothing
 */
export const changeMemberActivation = (id: string, formData: FormData) => async (
  dispatch: any
) => {
  try {
    dispatch({ type: MEMBERSHIP_REQUEST });

    await axios.put(`${API_URL}/users/${id}`, formData, optionsFormData);
    await Promise.all([dispatch(getMember(id)), dispatch(getMember())]);
  } catch (err) {
    console.error(err);
    dispatch({ type: MEMBERSHIP_FAILED, payload: err });
  }
};
