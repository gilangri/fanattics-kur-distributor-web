import {
  IMembershipAction,
  IMembershipState,
  MEMBERSHIP_FAILED,
  MEMBERSHIP_REQUEST,
  MEMBERSHIP_RESET,
  MEMBERSHIP_SEARCH,
  MEMBERSHIP_SEARCH_DONE,
  MEMBERSHIP_SEARCH_RESET,
  MEMBERSHIP_SET,
  MEMBERSHIP_SET_USER,
  MEMBERSHIP_SET_USERS,
  MEMBERSHIP_SET_VALUE,
} from "./types";

const init_state: IMembershipState = {
  users: [],
  user: undefined,
  filtered: [],

  searchQuery: "",
  searched: [],
  isSearching: false,

  loading: false,
  error: "",
};

export function membershipReducer(
  state = init_state,
  { payload, name, type }: IMembershipAction
): IMembershipState {
  switch (type) {
    case MEMBERSHIP_REQUEST:
      return { ...state, loading: true, error: "" };
    case MEMBERSHIP_FAILED:
      return { ...state, loading: false, error: payload };

    case MEMBERSHIP_SEARCH:
      return { ...state, isSearching: true };
    case MEMBERSHIP_SEARCH_DONE:
      return { ...state, searched: payload };
    case MEMBERSHIP_SEARCH_RESET:
      return { ...state, isSearching: false, searched: [] };

    case MEMBERSHIP_SET_USERS:
      return { ...state, loading: false, users: payload };
    case MEMBERSHIP_SET_USER:
      return { ...state, loading: false, user: payload };

    case MEMBERSHIP_SET:
      return { ...state, ...payload };
    case MEMBERSHIP_SET_VALUE:
      return { ...state, [name]: payload };

    case MEMBERSHIP_RESET:
      return init_state;
    default:
      return state;
  }
}
