import { AnyAction } from "redux";
import { IUser } from "../_interface/user";

export interface IMembershipState {
  users: IUser[];
  user?: IUser;

  filtered: IUser[];

  searchQuery: string;
  searched: IUser[];
  isSearching: boolean;

  loading: boolean;
  error: string;
}

export const MEMBERSHIP_REQUEST = "MEMBERSHIP_REQUEST";
export const MEMBERSHIP_FAILED = "MEMBERSHIP_FAILED";
export const MEMBERSHIP_RESET = "MEMBERSHIP_RESET";

export const MEMBERSHIP_SEARCH = "MEMBERSHIP_SEARCH";
export const MEMBERSHIP_SEARCH_DONE = "MEMBERSHIP_SEARCH_DONE";
export const MEMBERSHIP_SEARCH_RESET = "MEMBERSHIP_SEARCH_RESET";

export const MEMBERSHIP_SET_USERS = "MEMBERSHIP_SET_USERS";
export const MEMBERSHIP_SET_USER = "MEMBERSHIP_SET_USER";

export const MEMBERSHIP_SET = "MEMBERSHIP_SET";
export const MEMBERSHIP_SET_VALUE = "MEMBERSHIP_SET_VALUE";

export interface IMembershipAction extends AnyAction {
  payload: any;
  name: string;
  type:
    | typeof MEMBERSHIP_REQUEST
    | typeof MEMBERSHIP_FAILED
    | typeof MEMBERSHIP_RESET
    //
    | typeof MEMBERSHIP_SEARCH
    | typeof MEMBERSHIP_SEARCH_DONE
    | typeof MEMBERSHIP_SEARCH_RESET
    //
    | typeof MEMBERSHIP_SET_USER
    | typeof MEMBERSHIP_SET_USERS
    //
    | typeof MEMBERSHIP_SET
    | typeof MEMBERSHIP_SET_VALUE;
}
