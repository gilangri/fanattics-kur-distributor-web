import { Fragment, ReactNode } from "react";
import { Helmet } from "react-helmet";
import "./layout.scss";

interface Props {
  children: ReactNode;
  className?: string;
  title?: string;
}

/**
 * Container for primary page (page with sidebar)

 * Use it along with ContainerHead and ContainerBody is strongly recommended!

 * @param {ReactNode} children is a must!
 * @param {string} className (optional) default: parent-container
 * @param {string} title change the title on browser's tab
 */
export function ParentContainer({ children, className, title }: Props) {
  return (
    <Fragment>
      <Helmet>{title && <title>{title}</title>}</Helmet>
      <div className={`parent-container ${className}`}>{children}</div>
    </Fragment>
  );
}
