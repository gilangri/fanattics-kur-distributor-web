import { FC, ReactNode } from "react";
import "./layout.scss";

interface IBody {
  children: ReactNode;
  className?: string;
  orientation?: "horizontal" | "vertical";
}

/**
 * @param {ReactNode} children is a must!
 * @param {string} className (optional) default: container__body
 * @param {string} orientation (optional) default: vertical
 */
export function ContainerBody({
  children,
  className,
  orientation = "vertical",
}: IBody) {
  return (
    <div className={`container__body container__body-${orientation} ${className}`}>
      {children}
    </div>
  );
}

interface IBodyHead {
  className?: string;
}
/**
 *
 * @param children is a must!
 */
export const ContainerBodyHead: FC<IBodyHead> = ({ children, className }) => {
  return <div className={`body_head ${className}`}>{children}</div>;
};

interface IBodyContent {
  className?: string;
}
/**
 *
 * @param children is a must!
 * @param className put col-* to create more than one body container.
 *
 * PS: * stands for number for grid column (ex: col-3, col-9, etc)
 */
export const ContainerBodyContent: FC<IBodyContent> = ({ children, className }) => {
  return <div className={`body_content ${className}`}>{children}</div>;
};
