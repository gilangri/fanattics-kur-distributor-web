import { ITypography } from "./ITypography";

export default function MetadataText({
  children,
  accent,
  bold,
  className,
  color,
  style,
  ...props
}: ITypography) {
  return (
    <div
      {...props}
      style={{ color, ...style }}
      className={`component-typography metadata ${bold ? "bold" : ""} ${
        accent ? "accent" : ""
      } ${className}`}
    >
      {children}
    </div>
  );
}
