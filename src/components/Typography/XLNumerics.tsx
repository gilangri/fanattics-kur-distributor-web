import { ITypography } from "./ITypography";

export default function XLNumerics({
  children,
  accent,
  bold,
  className,
  color,
  style,
  ...props
}: ITypography) {
  return (
    <div
      {...props}
      style={{ color, ...style }}
      className={`component-typography xl-numerics ${bold ? "bold" : ""} ${
        accent ? "accent" : ""
      } ${className}`}
    >
      {children}
    </div>
  );
}
