import { Modal as MyModal } from "reactstrap";

interface IModal extends React.HTMLAttributes<HTMLElement> {
  [key: string]: any;
  isOpen?: boolean;
  children?: React.ReactNode;
  autoFocus?: boolean;
  size?: "sm" | "md" | "lg" | "xl" | "auto";
  toggle?: React.KeyboardEventHandler<any> | React.MouseEventHandler<any>;
  backdrop?: boolean | "static";
  scrollable?: boolean;
  className?: string;
  fade?: boolean;
  centered?: boolean;
  width?: number;
}

export function Modal({
  toggle,
  isOpen,
  backdrop,
  className = "",
  children,
  centered = true,
  fade,
  scrollable,
  size = "md",
}: IModal) {
  return (
    <MyModal
      backdrop={backdrop}
      centered={centered}
      fade={fade}
      scrollable={scrollable}
      className={`custom-modal custom-modal-${size} ${className}`}
      toggle={toggle}
      isOpen={isOpen}
    >
      {children}
    </MyModal>
  );
}
