import { ModalBody as MyModalBody } from "reactstrap";

interface IModal {
  className?: string;
  children: React.ReactNode;
}

export function ModalBody({ className, children }: IModal) {
  return <MyModalBody className={className}>{children}</MyModalBody>;
}
