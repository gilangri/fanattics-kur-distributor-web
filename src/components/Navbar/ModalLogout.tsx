import { Button } from "../Button";
import { Modal, ModalBody } from "../Modal";

interface Props {
  isOpen: boolean;
  onCancel: () => void;
  onConfirm: () => void;
}
export default function ModalLogout({ isOpen, onCancel, onConfirm }: Props) {
  return (
    <Modal centered isOpen={isOpen} size="sm">
      <ModalBody>
        <div className="text-center">Are you sure to logout?</div>
        <br />
        <div className="d-flex justify-content-center">
          <Button
            onClick={onConfirm}
            theme="secondary"
            className="mr-1"
            minWidth={75}
          >
            Yes
          </Button>
          <Button
            onClick={onCancel}
            theme="secondary"
            className="ml-1"
            minWidth={75}
          >
            No
          </Button>
        </div>
      </ModalBody>
    </Modal>
  );
}
