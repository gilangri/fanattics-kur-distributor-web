import { Fragment, useEffect, useState } from "react";
import { Select } from "../Select";
import LogoKur from "../../assets/images/kur-logo.png";
import { Notification16, Settings16, Power16 } from "@carbon/icons-react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import { AUTH_RESET } from "../../redux/auth/types";
import ModalLogout from "./ModalLogout";
import { RootState } from "../../redux/store";
import { SIDEBAR_ROLE } from "../../redux/sidebar/types";
import { adminRole } from "../../constants";
import { enumAdmin } from "../../utils";

export default function NavBar() {
  const dispatch = useDispatch();
  const history = useHistory();

  // GLOBAL STATE
  const user = useSelector((state: RootState) => state.auth.user);

  // LOCAL STATE
  const [selectedRole, setSelectedRole] = useState("");
  const [isLoggingOut, setIsLoggingOut] = useState(false);

  // FUNCTION
  const handleLogout = () => {
    // Cookies.remove("token");
    localStorage.removeItem("token");
    history.push("/login");
    dispatch({ type: AUTH_RESET });
  };

  // setting default selected role based on logged in user
  useEffect(() => user && setSelectedRole(user.role), [user]);

  // change navbar based on selected role
  useEffect(() => {
    const handleChangeRole = (value: string) => {
      dispatch({ type: SIDEBAR_ROLE, payload: value });
    };

    if (selectedRole === "super") return handleChangeRole("sales");
    handleChangeRole(selectedRole);
  }, [dispatch, selectedRole]);

  return (
    <Fragment>
      <ModalLogout
        isOpen={isLoggingOut}
        onCancel={() => setIsLoggingOut(false)}
        onConfirm={handleLogout}
      />

      <div className="navbar">
        <div className="navbar-flex-left">
          <div className="logo-kur">
            <img src={LogoKur} alt="" className="img-kur" />
          </div>
          <div className="divider" />
          <Select
            defaultValue={selectedRole}
            className="navbar-role-select"
            disabled
            // onChange={({ target }) => setSelectedRole(target.value)}
          >
            {user && user.role === "super" ? (
              adminRole.navbarOptions.map((value, i) => (
                <option key={i} value={value}>
                  {enumAdmin(value)}
                </option>
              ))
            ) : (
              <option value={selectedRole}>{enumAdmin(selectedRole)}</option>
            )}
          </Select>
        </div>

        <div className="navbar-flex-right">
          <div className="container-button">
            <button>
              <Notification16 />
            </button>
            <div className="divider" />
            <button>
              <Settings16 />
            </button>
            <div className="divider" />
            <button onClick={() => setIsLoggingOut(true)}>
              <Power16 />
            </button>
          </div>

          <div className="profile">
            <div className="image">
              <img
                src="https://images.pexels.com/photos/6355177/pexels-photo-6355177.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"
                alt="admin"
              />
            </div>
            <div className="divider" />
            <div className="name">Syafruddin </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
}
