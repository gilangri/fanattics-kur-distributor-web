import React from "react";
import { gray700 } from "../../constants";
import { MetadataText } from "../Typography";

export default function Label({
  children,
  style,
  bold = false,
}: {
  children: React.ReactNode;
  style?: React.CSSProperties;
  bold?: boolean;
}) {
  return (
    <MetadataText bold={bold} color={gray700} style={style}>
      {children}
    </MetadataText>
  );
}
