import { SidebarMenu } from "./SidebarMenu";
import { SidebarSubmenu } from "./SidebarSubmenu";

export default function Sales() {
  const slugOpen = "/sales";

  return (
    <div className="sidebar-kur">
      <SidebarMenu title="Transaksi">
        <SidebarSubmenu
          title="Transaksi Berlangsung"
          slug={`${slugOpen}/on-process`}
          badge={20}
        />
        <SidebarSubmenu
          title="Transaksi Selesai"
          slug={`${slugOpen}/profile-user`}
          badge={20}
        />
        <SidebarSubmenu title="Laporan Transaksi" badge={20} />
      </SidebarMenu>
      <SidebarMenu title="Member UMKM">
        <SidebarSubmenu title="Transaksi Berlangsung" badge={20} />
      </SidebarMenu>
    </div>
  );
}
