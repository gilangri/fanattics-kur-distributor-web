import {
  UilUserSquare,
  UilUserLocation,
  UilInvoice,
  UilTagAlt,
  UilStore,
  // Uilsettting,
} from "@iconscout/react-unicons";
import { SidebarMenu } from "./SidebarMenu";
import { SidebarSubmenu } from "./SidebarSubmenu";
import fanatticsLogo from "../../assets/images/fanattics-logo.png";
import { useHistory } from "react-router";
import { useDispatch } from "react-redux";
import { SIDEBAR_SELECT } from "../../redux/sidebar/types";
// import { AiOutlineSetting } from "react-icons/ai";

export default function Sidebar() {
  const dispatch = useDispatch();
  const history = useHistory();

  const slugTrx = "/transaction";
  const slugProduct = "/product";

  return (
    <div className="sidebar-kur">
      <div className="sidebar-wrapper">
        {/* ============================== PRODUK SAYA ==================== */}
        <SidebarMenu
          icon={<UilTagAlt />}
          title="Produk Saya"
          onClick={() => {
            let title = "Katalog Produk";
            let slug = `${slugProduct}/catalog`;
            dispatch({ type: SIDEBAR_SELECT, payload: { title, slug } });
            history.push(slug);
          }}
        >
          <SidebarSubmenu title="Katalog Produk" slug={`${slugProduct}/catalog`} />
          <SidebarSubmenu title="Riwayat Produk" slug={`${slugProduct}/history`} />
        </SidebarMenu>

        {/* ============================== TRANSAKSI ==================== */}
        <SidebarMenu icon={<UilInvoice />} title="Transaksi">
          <SidebarSubmenu
            title="Transaksi Berlangsung"
            slug={`${slugTrx}/on-process`}
            // badge={20}
          />
          <SidebarSubmenu
            title="Transaksi Selesai"
            slug={`${slugTrx}/done`}
            // badge={20}
          />
          <SidebarSubmenu
            title="Laporan Transaksi"
            slug={`${slugTrx}/reports`}
            // badge={20}
          />
        </SidebarMenu>

        {/* ============================== MEMBER ==================== */}
        <SidebarMenu
          icon={<UilUserLocation />}
          title="Member UMKM"
          slug="/membership"
        />

        {/* ============================== DISTRIBUTOR ==================== */}
        <SidebarMenu icon={<UilStore />} title="Distributor" slug="/distributor" />

        {/* ============================== ADMIN ==================== */}
        <SidebarMenu icon={<UilUserSquare />} title="Admin" slug="/admin" />

        {/* ============================== APPS SETTING & PROFILE ==================== */}
        {/* <SidebarMenu
          icon={<AiOutlineSetting />}
          title="Setting Profile"
          slug="/setting-profile"
        ></SidebarMenu> */}
      </div>

      <div className="logo-fanattics">
        <span>Powered By</span>
        <img src={fanatticsLogo} alt="" />
      </div>
    </div>
  );
}
