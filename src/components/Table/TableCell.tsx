import React from "react";

interface ITableCell extends React.HTMLAttributes<HTMLTableDataCellElement> {
  children?: React.ReactNode;
  colSpan?: number;
  align?: "left" | "center" | "right" | "justify" | "char";
}

export default function TableCell({
  children,
  colSpan,
  align,
  ...props
}: ITableCell) {
  return (
    <td {...props} colSpan={colSpan} align={align}>
      {children}
    </td>
  );
}
