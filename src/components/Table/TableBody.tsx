import React from "react";

interface ITableBody extends React.HTMLAttributes<HTMLTableSectionElement> {
  children: React.ReactNode;
  ref?: any;
}

export default function TableBody({ children, ref, ...props }: ITableBody) {
  return (
    <tbody ref={ref} {...props}>
      {children}
    </tbody>
  );
}
