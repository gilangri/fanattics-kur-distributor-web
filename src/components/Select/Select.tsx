import React, { ReactNode } from "react";

interface ISelect extends React.SelectHTMLAttributes<HTMLSelectElement> {
  children: ReactNode;
  disabled?: boolean;
  active?: boolean;
  className?: string;
}

export default function Select({ children, className, ...props }: ISelect) {
  return (
    <select {...props} className={`component-select ${className}`}>
      {children}
    </select>
  );
}
