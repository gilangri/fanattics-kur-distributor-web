/**
 * Rename technical words for transaction status to human language
 * @param status input transaction status
 * @returns renamed status
 */
export const enumTrxStatus = (status: string = "") => {
  if (status === "awaiting-confirmation") return "Menunggu Konfirmasi";
  if (status === "on-process") return "Dalam Proses";
  if (status === "done") return "Transaksi Selesai";
  if (status === "failed") return "Transaksi Gagal";
  if (status === "cancelled") return "Transaksi Dibatalkan";
  return "-";
};

/**
 * Rename technical words for delivery status to human language
 * @param status input delivery status
 * @returns renamed status
 */
export const enumDelivery = (status: string = "") => {
  if (status === "requested") return "Dalam permintaan";
  if (status === "paid") return "Sudah dibayar";
  if (status === "confirmed") return "Pembayaran dikonfirmasi";
  if (status === "on-delivery") return "Dalam pengiriman";
  if (status === "finished") return "Tiba di tujuan";
  if (status === "rejected") return "Ditolak";
  return "_";
};
