/// Change enumeration from technical term
/// to human language
export const enumAdmin = (value: string) => {
  if (value === "sales") return "Sales";
  if (value === "product") return "Product";
  if (value === "super") return "Super Admin";
  return "#N/A";
};
