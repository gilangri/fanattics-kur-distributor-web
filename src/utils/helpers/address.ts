import { IAddress } from "../../redux/_interface/address";

export const joinAddress = (v: IAddress | undefined | null = undefined) => {
  if (!v) return "";
  return `${v.address} ${v.subdistrict}, ${v.district}, ${v.city}, ${v.province}, ${v.postalCode}`;
};
