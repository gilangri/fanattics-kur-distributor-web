import { routeTypes } from "./types";
import Sidebar from "../components/Sidebar";
import DistributorPage from "../pages/Distributor";
import DistributorForm from "../pages/Distributor/Form";

const slug = "/distributor";

export const routeDistributor: routeTypes[] = [
  {
    exact: true,
    path: `${slug}`,
    sidebar: () => <Sidebar />,
    main: () => <DistributorPage />,
  },
  {
    exact: true,
    path: `${slug}/form`,
    sidebar: () => <Sidebar />,
    main: () => <DistributorForm />,
  },
];
