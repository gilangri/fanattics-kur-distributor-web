import { routeTypes } from "./types";
import Sidebar from "../components/Sidebar";
import AdminPage from "../pages/Admin";

const slug = "/admin";

export const routeAdmin: routeTypes[] = [
  {
    exact: true,
    path: `${slug}`,
    sidebar: () => <Sidebar />,
    main: () => <AdminPage />,
  },
];
