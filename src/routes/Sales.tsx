import { routeTypes } from "./types";
import Sidebar from "../components/Sidebar";
// import NewTransaction from "../pages/Sales/NewTransaction";
// import NewTransactionDetail from "../pages/Transaction/NewTransactionDetail";
import Transaction from "../pages/Sales";
import TransactionDetail from "../pages/Sales/Detail";

const slug = "/transaction";

export const routeSales: routeTypes[] = [
  {
    exact: true,
    path: `${slug}/on-process`,
    sidebar: () => <Sidebar />,
    main: () => <Transaction />,
  },
  {
    exact: true,
    path: `${slug}/done`,
    sidebar: () => <Sidebar />,
    main: () => <Transaction />,
  },
  {
    exact: true,
    path: `${slug}/detail`,
    main: () => <TransactionDetail />,
  },
  {
    exact: true,
    path: `${slug}/reports`,
    sidebar: () => <Sidebar />,
    main: () => null,
  },
];
