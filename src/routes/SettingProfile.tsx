import { routeTypes } from "./types";
import Sidebar from "../components/Sidebar";
import SettingProfile from "../pages/SettingProfile";

const slug = "/setting-profile";

export const routeSettingProfile: routeTypes[] = [
  {
    exact: true,
    path: `${slug}`,
    sidebar: () => <Sidebar />,
    main: () => <SettingProfile />,
  },
];
