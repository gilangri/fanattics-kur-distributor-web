export type routeTypes = {
  path: string;
  exact?: boolean;
  sidebar?: () => JSX.Element;
  main: () => JSX.Element | null;
};
  