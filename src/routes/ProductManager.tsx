import Sidebar from "../components/Sidebar";
import ProductManager from "../pages/ProductManager";
import ProfileUser from "../pages/ProductManager/ProfileUser";

import { routeTypes } from "./types";

const trxSlug = "/product/transaction";

export const routeProductManager: routeTypes[] = [
  {
    exact: true,
    path: `${trxSlug}/on-process`,
    sidebar: () => <Sidebar />,
    main: () => <ProductManager />,
  },
  {
    exact: true,
    path: `${trxSlug}/profile-user`,
    sidebar: () => <Sidebar />,
    main: () => <ProfileUser />,
  },
];
