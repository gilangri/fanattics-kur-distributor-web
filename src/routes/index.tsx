import { routeTypes } from "./types";
import { routeProduct } from "./Product";
import { routeSales } from "./Sales";
import Login from "../pages/Login";
import { routeMembership } from "./Membership";
import { routeAdmin } from "./Admin";
import { routeSettingProfile } from "./SettingProfile";
import { routeDistributor } from "./Distributor";

export const AppRoutes: routeTypes[] = [
  {
    exact: true,
    path: `/login`,
    main: () => <Login />,
  },
  ...routeProduct,
  ...routeSales,
  ...routeMembership,
  ...routeAdmin,
  ...routeDistributor,
  ...routeSettingProfile,
];
