import { routeTypes } from "./types";
import Sidebar from "../components/Sidebar";
import ProductCatalog from "../pages/Product";
import CategoryConfiguration from "../pages/Product/Category";
import AddNewProduct from "../pages/Product/AddProduct";
import ProductDetail from "../pages/Product/ProductDetail";

const slug = "/product";

export const routeProduct: routeTypes[] = [
  {
    exact: true,
    path: `${slug}/catalog`,
    sidebar: () => <Sidebar />,
    main: () => <ProductCatalog />,
  },
  {
    exact: true,
    path: `${slug}/categories`,
    main: () => <CategoryConfiguration />,
  },
  {
    exact: true,
    path: `${slug}/add-product`,
    main: () => <AddNewProduct />,
  },
  {
    exact: true,
    path: `${slug}/detail`,
    main: () => <ProductDetail />,
  },
  {
    exact: true,
    path: `${slug}/history`,
    sidebar: () => <Sidebar />,
    main: () => null,
  },
];
