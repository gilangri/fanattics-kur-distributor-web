import { routeTypes } from "./types";
import Sidebar from "../components/Sidebar";
import Membership from "../pages/Membership";
import MembershipDetail from "../pages/Membership/ProfileDetail";

const slug = "/membership";

export const routeMembership: routeTypes[] = [
  {
    exact: true,
    path: `${slug}`,
    sidebar: () => <Sidebar />,
    main: () => <Membership />,
  },
  {
    exact: true,
    path: `${slug}/detail`,
    main: () => <MembershipDetail />,
  },
];
